[Desktop Entry]
Name=pt-peptideviewer
Categories=Education;Science;Math;
Comment=PAPPSO tools peptide viewer
Exec=${CMAKE_INSTALL_PREFIX}/bin/pt-peptideviewer
Icon=${CMAKE_INSTALL_PREFIX}/share/pappsoms-tools/pappsoms-tools.svg
Terminal=false
Type=Application
StartupNotify=true