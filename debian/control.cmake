Source: pappsoms-tools
Maintainer: Olivier Langella <olivier.langella@u-psud.fr>
Homepage: http://pappso.inra.fr/bioinfo
Section: libs
Priority: optional
Build-Depends: debhelper (>= 9),
               cmake (>= 2.6),
               qt5-default,
               libpappsomspp-widget-dev (= @LIBPAPPSOMSPP_VERSION@),
               libodsstream-qt5-dev,
               libqt5svg5-dev
Standards-Version: 3.9.4

Package: pappsoms-tools
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libpappsomspp-widget0 (= @LIBPAPPSOMSPP_VERSION@),
         libodsstream-qt5-0,
         libqt5core5a,
         libqt5gui5,
         libqt5svg5,
         libqt5xml5
Pre-Depends: ${misc:Pre-Depends}
Description: set of proteomic tools used on PAPPSO. computes peptide mass, isotope ratio,
 fragmentation products, protein grouper.
