
# cd buildwin64
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64/home.cmake ..

set(ODSSTREAM_QT5_FOUND 1)
set(ODSSTREAM_INCLUDE_DIR "/home/olivier/eclipse/git/libodsstream/src")
set(ODSSTREAM_QT5_LIBRARY "/home/olivier/eclipse/git/libodsstream/build/src/libodsstream-qt5.so")

set(PAPPSOMSPP_QT5_FOUND 1)
set(PAPPSOMSPP_WIDGET_QT5_FOUND 1)
set(PAPPSOMSPP_INCLUDE_DIR "/home/olivier/eclipse/git/pappsomspp/src")
set(PAPPSOMSPP_QT5_LIBRARY "/home/olivier/eclipse/git/pappsomspp/cbuild/src/libpappsomspp-qt5.so")
set(PAPPSOMSPP_WIDGET_QT5_LIBRARY "/home/olivier/eclipse/git/pappsomspp/cbuild/src/pappsomspp/widget/libpappsomspp-widget-qt5.so")



if(NOT TARGET Pappso::Core)
        add_library(Pappso::Core UNKNOWN IMPORTED)
        set_target_properties(Pappso::Core PROPERTIES
            IMPORTED_LOCATION             "${PAPPSOMSPP_QT5_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${PAPPSOMSPP_INCLUDE_DIR}")
endif()


if(NOT TARGET Pappso::Widget)
        add_library(Pappso::Widget UNKNOWN IMPORTED)
        set_target_properties(Pappso::Widget PROPERTIES
            IMPORTED_LOCATION             "${PAPPSOMSPP_WIDGET_QT5_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${PAPPSOMSPP_INCLUDE_DIR}")
endif()


   
if(NOT TARGET OdsStream::Core)
    add_library(OdsStream::Core UNKNOWN IMPORTED)
    set_target_properties(OdsStream::Core PROPERTIES
            IMPORTED_LOCATION             "${ODSSTREAM_QT5_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${ODSSTREAM_INCLUDE_DIR}"
    )
endif()
