
# cd build
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64/qcustomplot2.cmake ..


set(PAPPSOMSPP_QT5_FOUND 1)
set(PAPPSOMSPP_WIDGET_QT5_FOUND 1)
set(PAPPSOMSPP_INCLUDE_DIR "/home/olivier/eclipse/git/pappsomspp/src")
set(PAPPSOMSPP_QT5_LIBRARY "/home/olivier/eclipse/git/pappsomspp/cbuild/src/libpappsomspp-qt5.so")
set(PAPPSOMSPP_WIDGET_QT5_LIBRARY "/home/olivier/eclipse/git/pappsomspp/cbuild/src/pappsomspp/widget/libpappsomspp-widget-qt5.so")


set (QCustomPlot_FOUND 1)
set (QCustomPlot_INCLUDES "/home/olivier/eclipse/git/qcustomplot-2.0.1+dfsg1")
set (QCustomPlot_LIBRARIES "/home/olivier/eclipse/git/qcustomplot-2.0.1+dfsg1/build/libqcustomplot.so")

