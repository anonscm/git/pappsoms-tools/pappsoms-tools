
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ptpeptideions.h"


#include <iostream>
#include <QCoreApplication>
#include <QTimer>
#include <QDebug>
#include <QDateTime>
#include <QDir>
#include <QCommandLineParser>
#include <QSvgGenerator>
#include <QPainter>
#include <QBuffer>
#include <QSvgRenderer>
#include <QGraphicsSvgItem>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsScene>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/psm/peptidespectrummatch.h>
#include <pappsomspp/psm/xtandem/xtandemhyperscore.h>
#include <pappsomspp/psm/xtandem/xtandemspectrumprocess.h>
#include <pappsomspp/peptide/peptidestrparser.h>
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvdirectorywriter.h>
#include <odsstream/tsvoutputstream.h>
#include <vector>
#include <pappsomspp/widget/massspectrumwidget/massspectrumwidget.h>
#include <pappsomspp/msfile/msfileaccessor.h>

using namespace pappso;

void
PtPeptideIons::saveSvg(const QString &filename,
                       const pappso::PeptideSp &peptideSp,
                       const pappso::MassSpectrumSPtr &spectrumSp,
                       unsigned int zmax,
                       pappso::PrecisionPtr precision,
                       std::list<pappso::PeptideIon> ion_list)
{
  // QByteArray byteArray;
  //  QBuffer buffer(&byteArray);


  // QPainter painter;
  // painter.begin(&generator);
  MassSpectrumWidget svg_spectrum;
  svg_spectrum.setMassSpectrumCstSPtr(spectrumSp);
  svg_spectrum.setPeptideSp(peptideSp);
  svg_spectrum.setPeptideCharge(zmax);
  svg_spectrum.setMs2Precision(precision);
  svg_spectrum.setIonList(ion_list);
  // svg_spectrum.render(&painter);

  // painter.end();

  svg_spectrum.toSvgFile(filename,
                         tr("SVG Generator Example Drawing"),
                         tr("An SVG drawing created by the SVG Generator "
                            "Example provided with Qt."),
                         QSize(1200, 500));
  /*
  QSvgRenderer renderer;
  renderer.load(byteArray);
  QGraphicsSvgItem graphic_svg;
  graphic_svg.setSharedRenderer(& renderer);
  //graphic_svg.setScale(0.5);
  graphic_svg.setRotation(45);



     QSvgGenerator generator2;
  generator2.setFileName(filename);

  generator2.setSize(QSize(200, 200));
  generator2.setViewBox(QRect(0, 0, 200, 200));
  generator2.setTitle(tr("SVG Generator Example Drawing"));
  generator2.setDescription(tr("An SVG drawing created by the SVG Generator "
                             "Example provided with Qt."));

  QGraphicsScene * scene = new QGraphicsScene(QRectF(0,0,200,200));

  //delete scene;
  scene->addText("Hello, world!");


  QPainter painter2;
  painter2.begin(&generator2);
  // scene.setFont(QFont ("Courier", 9).setPointSize(1));
  scene->addItem(&graphic_svg);
  scene->render(&painter2);
  //painter2.drawRect(0,0,200,200);

  painter2.end();
  */
}

void
writeOneIon(CalcWriterInterface &writer,
            const OdsTableCellStyleRef &styleNoMatch,
            std::map<PeptideIon, OdsTableCellStyleRef> &map_ion_colors,
            const PeptideFragmentIonSp &peptideFragmentIonSp,
            const PeptideSpectrumMatch *p_psm,
            const unsigned int &z)
{

  writer.setTableCellStyleRef(styleNoMatch);
  if(p_psm != nullptr)
    {
      if(p_psm->contains(peptideFragmentIonSp.get(), z))
        {
          writer.setTableCellStyleRef(
            map_ion_colors.at(peptideFragmentIonSp.get()->getPeptideIonType()));
        }
      else
        {
          // writer.setTableCellStyleRef();
        }
    }
  writer.writeCell(peptideFragmentIonSp.get()->getMz(z));
  writer.clearTableCellStyleRef();
}

void
PtPeptideIons::writePeptideIonTable(CalcWriterInterface &writer,
                                    const pappso::PeptideSp &peptideSp,
                                    const pappso::MassSpectrumSPtr &spectrumSp,
                                    unsigned int zmax,
                                    pappso::PrecisionPtr precision,
                                    std::list<pappso::PeptideIon> ion_list)
{
  // unsigned int zmax = 4;
  // DaltonPrecision precision(0.5);
  // std::list<PeptideIon> ion_list =
  // PeptideFragmentIonListBase::getCIDionList();

  OdsTableCellStyle style;
  style.setBackgroundColor(QColor("grey"));

  OdsTableCellStyleRef styleNoMatch = writer.getTableCellStyleRef(style);
  // std::list<PeptideIon> ion_list =
  // PeptideFragmentIonListBase::getCIDionList();

  style.setBackgroundColor(QColor());
  std::map<PeptideIon, OdsTableCellStyleRef> _map_ion_colors;
  for(auto ion_type : ion_list)
    {
      style.setTextColor(PeptideFragmentIon::getPeptideIonColor(ion_type));

      _map_ion_colors.insert(std::pair<PeptideIon, OdsTableCellStyleRef>(
        ion_type, writer.getTableCellStyleRef(style)));
    }


  writer.writeSheet("peptide");
  writer.writeCell("sequence");
  writer.writeCell(peptideSp.get()->getSequence());
  writer.writeLine();
  writer.writeCell("formula");
  writer.writeCell(peptideSp.get()->getFormula(0));
  writer.writeLine();
  writer.writeCell("mass");
  writer.writeCell(peptideSp.get()->getMass());
  writer.writeLine();
  writer.writeCell("mz (z=1)");
  writer.writeCell(peptideSp.get()->getMz(1));
  writer.writeLine();
  writer.writeCell("mz (z=2)");
  writer.writeCell(peptideSp.get()->getMz(2));
  writer.writeLine();
  writer.writeCell("mz (z=3)");
  writer.writeCell(peptideSp.get()->getMz(3));

  PeptideFragmentIonListBase peptide_ion_list(peptideSp, ion_list);
  PeptideSpectrumMatch *p_psm = nullptr;
  if(spectrumSp.get() != nullptr)
    {
      p_psm = new PeptideSpectrumMatch(
        *spectrumSp.get(), peptide_ion_list, zmax, precision, ion_list);

      XtandemSpectrumProcess spectrum_process;
      spectrum_process.setExcludeParent(false);
      spectrum_process.setExcludeParentNeutralLoss(false);

      MassSpectrum spectrum_for_hyperscore =
        spectrum_process.process(*spectrumSp.get(), 0, 0);

      XtandemHyperscore hyperscore_withxtspectrum(
        spectrum_for_hyperscore, peptideSp, zmax, precision, ion_list, true);

      writer.writeLine();
      writer.writeCell("hyperscore");
      writer.writeCell(hyperscore_withxtspectrum.getHyperscore());
    }


  writer.writeSheet("ion table");
  writer.writeCell(peptideSp.get()->getSequence());
  writer.writeLine();
  unsigned int phosphorylation_number = 0;
  AaModificationP phosphorylation_mod = nullptr;
  phosphorylation_mod = AaModification::getInstance("MOD:00696");
  phosphorylation_number =
    peptideSp.get()->getNumberOfModification(phosphorylation_mod);


  writer.writeCell("Nter size");
  for(auto ion_type : ion_list)
    {
      PeptideDirection ion_direction =
        PeptideFragmentIon::getPeptideIonDirection(ion_type);
      if(ion_direction == PeptideDirection::Nter)
        {
          for(unsigned int z = 1; z <= zmax; z++)
            {
              if(ion_type != PeptideIon::bp)
                {
                  writer.writeCell(
                    QString("%1 (z=%2)")
                      .arg(PeptideFragmentIon::getPeptideIonName(ion_type))
                      .arg(z));
                }
              else
                {
                  for(unsigned int i = 0; i < phosphorylation_number; i++)
                    {
                      writer.writeCell(
                        QString("%1%2 (z=%3)")
                          .arg(PeptideFragmentIon::getPeptideIonName(ion_type))
                          .arg(i + 1)
                          .arg(z));
                    }
                }
            }
        }
    }
  writer.writeCell("Sequence");
  for(auto ion_type : ion_list)
    {
      PeptideDirection ion_direction =
        PeptideFragmentIon::getPeptideIonDirection(ion_type);
      if(ion_direction == PeptideDirection::Cter)
        {
          for(unsigned int z = 1; z <= zmax; z++)
            {
              if(ion_type != PeptideIon::yp)
                {
                  writer.writeCell(
                    QString("%1 (z=%2)")
                      .arg(PeptideFragmentIon::getPeptideIonName(ion_type))
                      .arg(z));
                }
              else
                {
                  for(unsigned int i = 0; i < phosphorylation_number; i++)
                    {
                      writer.writeCell(
                        QString("%1%2 (z=%3)")
                          .arg(PeptideFragmentIon::getPeptideIonName(ion_type))
                          .arg(i + 1)
                          .arg(z));
                    }
                }
            }
        }
    }
  writer.writeCell("Cter size");
  writer.writeLine();


  for(unsigned int i = 0; i < peptideSp.get()->size(); i++)
    {
      unsigned int ctersize = peptideSp.get()->size() - i;
      if(ctersize == 1)
        {
          writer.writeEmptyCell();
        }
      else
        {
          writer.writeCell(i + 1);
        }
      for(auto ion_type : ion_list)
        {
          PeptideDirection ion_direction =
            PeptideFragmentIon::getPeptideIonDirection(ion_type);
          if(ion_direction == PeptideDirection::Nter)
            {
              for(unsigned int z = 1; z <= zmax; z++)
                {
                  if(i == peptideSp.get()->size() - 1)
                    {
                      if(ion_type == PeptideIon::bp)
                        {
                          for(unsigned int ip = 0; i < phosphorylation_number;
                              i++)
                            {
                              writer.writeEmptyCell();
                            }
                        }
                      else
                        {
                          writer.writeEmptyCell();
                        }
                    }
                  else
                    {

                      if(ion_type == PeptideIon::bp)
                        {
                          for(unsigned int ip = 0; i < phosphorylation_number;
                              i++)
                            {
                              PeptideFragmentIonSp peptideFragmentIonSp =
                                peptide_ion_list.getPeptideFragmentIonSp(
                                  ion_type, i + 1, ip + 1);
                              writeOneIon(writer,
                                          styleNoMatch,
                                          _map_ion_colors,
                                          peptideFragmentIonSp,
                                          p_psm,
                                          z);
                            }
                        }
                      else
                        {
                          PeptideFragmentIonSp peptideFragmentIonSp =
                            peptide_ion_list.getPeptideFragmentIonSp(ion_type,
                                                                     i + 1);
                          writeOneIon(writer,
                                      styleNoMatch,
                                      _map_ion_colors,
                                      peptideFragmentIonSp,
                                      p_psm,
                                      z);
                        }
                    }
                }
            }
        }
      writer.writeCell(peptideSp.get()->getSequence().at(i));
      for(auto ion_type : ion_list)
        {
          PeptideDirection ion_direction =
            PeptideFragmentIon::getPeptideIonDirection(ion_type);
          if(ion_direction == PeptideDirection::Cter)
            {
              for(unsigned int z = 1; z <= zmax; z++)
                {
                  if(i == 0)
                    {

                      if(ion_type == PeptideIon::yp)
                        {
                          for(unsigned int ip = 0; i < phosphorylation_number;
                              i++)
                            {
                              writer.writeEmptyCell();
                            }
                        }
                      else
                        {
                          writer.writeEmptyCell();
                        }
                    }
                  else
                    {
                      if(ion_type == PeptideIon::yp)
                        {
                          for(unsigned int ip = 0; i < phosphorylation_number;
                              i++)
                            {
                              PeptideFragmentIonSp peptideFragmentIonSp =
                                peptide_ion_list.getPeptideFragmentIonSp(
                                  ion_type, ctersize, ip + 1);
                              writeOneIon(writer,
                                          styleNoMatch,
                                          _map_ion_colors,
                                          peptideFragmentIonSp,
                                          p_psm,
                                          z);
                            }
                        }
                      else
                        {
                          PeptideFragmentIonSp peptideFragmentIonSp =
                            peptide_ion_list.getPeptideFragmentIonSp(ion_type,
                                                                     ctersize);
                          writeOneIon(writer,
                                      styleNoMatch,
                                      _map_ion_colors,
                                      peptideFragmentIonSp,
                                      p_psm,
                                      z);
                        }
                    }
                }
            }
        }
      if(i == 0)
        {
          writer.writeEmptyCell();
        }
      else
        {
          writer.writeCell(ctersize);
        }
      writer.writeLine();
    }
}

PtPeptideIons::PtPeptideIons(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}


// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
PtPeptideIons::run()
{
  //./src/pt-peptideion LLDEFLIVK
  //./src/pt-peptideions LLDEFLIVK -o ion.ods -i
  //../../pappsomspp/test/data/peaklist_15046.mgf -s 0

  //./src/pt-peptideions AIADGSLLDLLR -o ion.ods -i
  //../../pappsomspp/test/data/peaklist_15046.mgf -s 0

  /// gorgone/pappso/formation/Janvier2014/TD/mzXML/20120906_balliau_extract_1_A01_urnb-1.mzXML
  // YTEVKPALTNMVSATK
  // 8141
  //./src/pt-peptideions YTEVKPALTNMVSATK -o ion.ods -i
  /// gorgone/pappso/formation/Janvier2014/TD/mzXML/20120906_balliau_extract_1_A01_urnb-1.mzXML
  //-s 8141 --svg test.svg

  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qDebug() << "PtPeptideMass::run() begin";
      QCommandLineParser parser;

      parser.setApplicationDescription(
        QString(SOFTWARE_NAME)
          .append(" ")
          .append(PAPPSOMSTOOLS_VERSION)
          .append(" peptide ion mass calculator.\nDisplays peptide and "
                  "fragmentation products masses."));
      parser.addHelpOption();
      parser.addVersionOption();
      parser.addPositionalArgument(
        "peptide", QCoreApplication::translate("main", "peptide sequence"));
      QCommandLineOption msData(
        QStringList() << "i"
                      << "MS data",
        QCoreApplication::translate("main", "MS data file (optional) <input>."),
        QCoreApplication::translate("main", "input"));
      QCommandLineOption scanNum(
        QStringList() << "s"
                      << "scan number",
        QCoreApplication::translate(
          "main",
          "scan number to extract an MS/MS spectrum from MS data file input "
          "(optional) <scan>."),
        QCoreApplication::translate("main", "scan"));
      QCommandLineOption odsOption(
        QStringList() << "o"
                      << "ods",
        QCoreApplication::translate("main",
                                    "Write results in an ODS file <output>."),
        QCoreApplication::translate("main", "output"));
      QCommandLineOption tsvOption(
        QStringList() << "t"
                      << "tsv",
        QCoreApplication::translate(
          "main", "Write results in TSV files contained in a <directory>."),
        QCoreApplication::translate("main", "directory"),
        QString());
      QCommandLineOption svgOption(
        QStringList() << "svg",
        QCoreApplication::translate("main", "Write SVG file in a <filename>."),
        QCoreApplication::translate("main", "SVG filename"),
        QString());
      parser.addOption(odsOption);
      parser.addOption(tsvOption);
      parser.addOption(msData);
      parser.addOption(scanNum);
      parser.addOption(svgOption);

      unsigned int zmax      = 4;
      PrecisionPtr precision = pappso::PrecisionFactory::getDaltonInstance(0.5);


      qDebug() << "PtPeptideMass::run() 1";

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << "PtPeptideMass.Run is executing";

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QFileInfo masschroqprm_dir_path(
        QCoreApplication::applicationDirPath());
      const QStringList args = parser.positionalArguments();
      QString peptideStr(args.at(0));

      PeptideSp peptideSp = PeptideStrParser::parseString(peptideStr);

      MassSpectrumSPtr spectrumSp;
      qDebug() << "spectrumSp.get() " << spectrumSp.get();
      if(!parser.value(msData).isEmpty())
        {
          if(!parser.value(scanNum).isEmpty())
            {
              MsFileAccessor accessor(parser.value(msData), "");
              MsRunReaderSPtr reader =
                accessor.getMsRunReaderSPtrByRunId("", "msrun");
              MassSpectrumSPtr spectrum_sp =
                reader->massSpectrumSPtr(parser.value(scanNum).toUInt());
            }
        }

      if(spectrumSp.get() != nullptr)
        {
          qDebug() << "spectrumSp.get() " << spectrumSp.get();
          qDebug() << "spectrumSp debugPrintValues "
                   << spectrumSp.get()->size();
          spectrumSp.get()->debugPrintValues();
        }

      QTextStream outputStream(stdout, QIODevice::WriteOnly);
      outputStream.setRealNumberPrecision(10);
      TsvOutputStream writer(outputStream);

      std::list<PeptideIon> ion_list =
        PeptideFragmentIonListBase::getCIDionList();

      writePeptideIonTable(
        writer, peptideSp, spectrumSp, zmax, precision, ion_list);

      QString odsFileStr = parser.value(odsOption);
      if(!odsFileStr.isEmpty())
        {
          QFile file(odsFileStr);
          // file.open(QIODevice::WriteOnly);
          OdsDocWriter writer(&file);
          // quantificator.report(writer);
          writePeptideIonTable(
            writer, peptideSp, spectrumSp, zmax, precision, ion_list);
          writer.close();
          file.close();
        }
      QString tsvFileStr = parser.value(tsvOption);
      if(!tsvFileStr.isEmpty())
        {
          QDir directory(tsvFileStr);
          qDebug() << "QDir directory(tsvFileStr) " << directory.absolutePath()
                   << " " << tsvFileStr;
          // file.open(QIODevice::WriteOnly);
          TsvDirectoryWriter writer(directory);
          writePeptideIonTable(
            writer, peptideSp, spectrumSp, zmax, precision, ion_list);
          // quantificator.report(writer);
          writer.close();
        }
      QString svgFileStr = parser.value(svgOption);
      if(!svgFileStr.isEmpty())
        {
          saveSvg(svgFileStr, peptideSp, spectrumSp, zmax, precision, ion_list);
        }

      // readMzXml();
      qDebug() << "MasschroqPrmCli::run() end";
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.what() << endl;
      exit(1);
      app->exit(1);
    }

  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
PtPeptideIons::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
PtPeptideIons::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{

  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << "main begin";
  QApplication app(argc, argv);

  qDebug() << "main 1";
  QApplication::setApplicationName("pt-peptidemass");
  QApplication::setApplicationVersion(PAPPSOMSTOOLS_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  PtPeptideIons myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug() << "main 2";


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
