/**
 * \file lib/xtandempresetsaxhandler.h
 * \date 06/02/2020
 * \author Olivier Langella
 * \brief read tandem preset file to get centroid parameters and number of
 * threads
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QXmlDefaultHandler>
#include <QXmlStreamWriter>
/**
 * @todo write docs
 */
class XtandemPresetSaxHandler : public QXmlDefaultHandler
{
  public:
  /**
   * Default constructor
   */
  XtandemPresetSaxHandler();

  /**
   * Destructor
   */
  ~XtandemPresetSaxHandler();

  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes) override;

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName) override;

  bool startDocument() override;

  bool endDocument() override;

  bool characters(const QString &str) override;

  bool fatalError(const QXmlParseException &exception) override;
  bool error(const QXmlParseException &exception) override;

  QString errorString() const;

  int getNumberOfThreads() const;
  const QString &getCentroidOptions() const;

  private:
  bool startElement_note(QXmlAttributes attributes);
  bool endElement_note();

  private:
  QString m_errorString;
  std::vector<QString> m_tagStack;
  QString m_currentText;
  bool m_isTandemParameter = false;
  QString m_currentLabel;
  QString m_centroidOptions;
  int m_threads = -1;
};
