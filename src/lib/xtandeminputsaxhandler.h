/**
 * \file lib/xtandeminputsaxhandler.h
 * \date 25/01/2020
 * \author Olivier Langella
 * \brief rewrites tandem xml input file with temporary files
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QXmlDefaultHandler>
#include <QXmlStreamWriter>
/**
 * @todo write docs
 */
class XtandemInputSaxHandler : public QXmlDefaultHandler
{
  public:
  /**
   * Default constructor
   */
  XtandemInputSaxHandler(const QString &destinationMzXmlFile,
                         const QString &destinationTandemInputFile,
                         const QString &destinationTandemOutputFile);

  /**
   * Destructor
   */
  ~XtandemInputSaxHandler();


  bool startElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName,
                    const QXmlAttributes &attributes) override;

  bool endElement(const QString &namespaceURI,
                  const QString &localName,
                  const QString &qName) override;

  bool startDocument() override;

  bool endDocument() override;

  bool characters(const QString &str) override;

  bool fatalError(const QXmlParseException &exception) override;
  bool error(const QXmlParseException &exception) override;

  QString errorString() const;

  const QString &getOriginalMsDataFileName() const;
  const QString &getOriginalTandemOutputFileName() const;
  
  const QString &getOriginalTandemPresetFileName() const;

  private:
  bool startElement_note(QXmlAttributes attributes);
  bool endElement_note();
  void writeOpenTag(const QString &qName, const QXmlAttributes &attributes);


  private:
  QString m_destinationMzXmlFileName;
  QString m_originMzDataFileName;
  QString m_destinationTandemInputFileName;
  QString m_originTandemPresetFileName;
  QString m_originTandemOutpuFileName;
  QString m_destinationTandemOutputFileName;
  QFile m_destinationTandemInputFile;
  QXmlStreamWriter *p_writeXmlTandemInput;
  std::vector<QString> m_tagStack;
  bool m_isTandemParameter = false;
  QString m_errorString;
  QString m_currentText;
  QString m_currentLabel;
  QString m_labelName;
};
