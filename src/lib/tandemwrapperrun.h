/**
 * \file lib/tandemwrapperrun.h
 * \date 25/01/2020
 * \author Olivier Langella
 * \brief actually does really run tandem directly on Bruker's data
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QDebug>
#include <QObject>
#include <QProcess>
#include <QTemporaryDir>

/**
 * @todo write docs
 */
class TandemWrapperRun : public QObject
{
  Q_OBJECT
  public:
  /** @brief prepare a tandem run
   * @param tandem_binary file path to tandem.exe if not set, a default value is
   * given in QSettings
   * @param tmp_dir temporary directory, where to write mzXML file conversion if
   * not set, a default value is given in QSettings
   * @param centroid_options controls the way MS2 spectrum are centroided from
   * raw bruker's data if not set, a default value is given in QSettings
   */
  TandemWrapperRun(const QString &tandem_binary,
                   const QString &tmp_dir,
                   const QString &centroid_options);

  /** @brief run a tandem job
   * @param tandem_input_file tandem xml input file
   * @param output_stream standard output where to write tandem stdout
   * @param error_stream standard error where to write tandem stderr
   */
  void run(const QString &tandem_input_file,
           QTextStream &output_stream,
           QTextStream &error_stream);

  void readTandemPresetFile(const QString &tandem_preset_file);
  /**
   * Destructor
   */
  ~TandemWrapperRun();

  private:
  void setTandemBinaryPath(const QString &tandem_binary_path);
  bool shouldIstop();
  const QString checkXtandemVersion(const QString &tandem_bin_path);
  void wrapTandemInputFile(const QString &tandem_input_file);

  void convertOrginalMsData2mzXmlData(const QString &origin,
                                      const QString &target) const;


  /** @brief run a tandem job
   * @param tandem_input_file tandem xml input file
   */
  void runTandem(const QString &tandem_input_file);

  /** @brief tandem output modification
   * tandem output is modified to contain the Bruker's file as input and
   * centroidization parameters
   * @param tmp_tandem_output raw tandem output filename
   * @param final_tandem_output final destination file for modified tandem
   * output
   */
  void writeFinalTandemOutput(const QString &tmp_tandem_output,
                              const QString &final_tandem_output,
                              const QString &original_msdata_file_name);


  private slots:
  void readyReadStandardOutput();
  void readyReadStandardError();

  private:
  QString m_tandemBinary;
  QString m_tandemVersion;
  QString m_tmpDir;
  QString m_centroidOptions;
  int m_maxTandemRunTimeMs = (60000 * 60 * 24); // 1 day
  QProcess *m_xtProcess    = nullptr;

  QTextStream *mp_outputStream = nullptr;
  QTextStream *mp_errorStream  = nullptr;

  QTemporaryDir *mpa_temporaryDirectory = nullptr;
};
