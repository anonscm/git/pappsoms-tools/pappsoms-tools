/**
 * \file lib/tandemwrapperrun.cpp
 * \date 25/01/2020
 * \author Olivier Langella
 * \brief actually does really run tandem directly on Bruker's data
 */

/*******************************************************************************
 * Copyright (c) 2020 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "tandemwrapperrun.h"
#include <QDebug>
#include <QFileInfo>
#include <QSettings>
#include <QThread>
#include <QThreadPool>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/private/timsmsrunreaderms2.h>
#include <pappsomspp/processing/filters/filterpseudocentroid.h>
#include <pappsomspp/msrun/mzxmloutput.h>
#include "xtandeminputsaxhandler.h"
#include "xtandemoutputsaxhandler.h"
#include "xtandempresetsaxhandler.h"

TandemWrapperRun::TandemWrapperRun(const QString &tandem_binary,
                                   const QString &tmp_dir,
                                   const QString &centroid_options)
{

  setTandemBinaryPath(tandem_binary);
  m_centroidOptions = centroid_options;
  if(m_centroidOptions.isEmpty())
    {
      m_centroidOptions = "20000 1. 3 10.";
    }

  if(tmp_dir.isEmpty())
    {
      mpa_temporaryDirectory = new QTemporaryDir(tmp_dir);
    }
  else
    {
      mpa_temporaryDirectory = new QTemporaryDir(QDir::tempPath());
    }
}

TandemWrapperRun::~TandemWrapperRun()
{
  if(mpa_temporaryDirectory != nullptr)
    {
      delete mpa_temporaryDirectory;
    }
}

void
TandemWrapperRun::setTandemBinaryPath(const QString &tandem_binary_path)
{

  m_tandemBinary = tandem_binary_path;
  QSettings settings;
  if(m_tandemBinary.isEmpty())
    {
      m_tandemBinary =
        settings.value("path/tandem_binary", "/usr/bin/tandem").toString();
    }
  // check for tandem executable
  m_tandemVersion = checkXtandemVersion(m_tandemBinary);

  qDebug() << m_tandemVersion;
  settings.setValue("path/tandem_binary", m_tandemBinary);
}


const QString
TandemWrapperRun::checkXtandemVersion(const QString &tandem_bin_path)
{
  qDebug();
  // check tandem path
  QFileInfo tandem_exe(tandem_bin_path);
  if(!tandem_exe.exists())
    {
      // dir.path() returns the unique directory path
      throw pappso::PappsoException(
        QObject::tr(
          "X!Tandem software not found at %1.\nPlease check the X!Tandem "
          "installation on your computer and set tandem.exe path.")
          .arg(tandem_exe.absoluteFilePath()));
    }
  if(!tandem_exe.isReadable())
    {
      // dir.path() returns the unique directory path
      throw pappso::PappsoException(
        QObject::tr("Please check permissions on X!Tandem software found at %1 "
                    "(file not readable).")
          .arg(tandem_exe.absoluteFilePath()));
    }
  if(!tandem_exe.isExecutable())
    {
      // dir.path() returns the unique directory path
      throw pappso::PappsoException(
        QObject::tr("Please check permissions on X!Tandem software found at %1 "
                    "(file not executable).")
          .arg(tandem_exe.absoluteFilePath()));
    }


  QString version_return;
  QStringList arguments;

  arguments << "-v";

  QProcess *xt_process = new QProcess();
  // hk_process->setWorkingDirectory(QFileInfo(_hardklor_exe).absolutePath());

  xt_process->start(tandem_bin_path, arguments);

  if(!xt_process->waitForStarted())
    {
      throw pappso::PappsoException(
        QObject::tr("X!Tandem %1 process failed to start")
          .arg(m_tandemVersion));
    }

  while(xt_process->waitForReadyRead(1000))
    {
    }
  /*
  if (!xt_process->waitForFinished(_max_xt_time_ms)) {
      throw pappso::PappsoException(QObject::tr("can't wait for X!Tandem process
  to finish : timeout at %1").arg(_max_xt_time_ms));
  }
  */
  QByteArray result = xt_process->readAll();


  qDebug() << result.constData();

  // X! TANDEM Jackhammer TPP (2013.06.15.1 - LabKey, Insilicos, ISB)

  QRegExp parse_version("(.*) TANDEM ([A-Z,a-z, ]+) \\(([^ ,^\\)]*)(.*)");
  qDebug() << parse_version;
  // Pattern patt = Pattern.compile("X! TANDEM [A-Z]+ \\((.*)\\)",
  //			Pattern.CASE_INSENSITIVE);

  if(parse_version.exactMatch(result.constData()))
    {
      version_return = QString("X!Tandem %1 %2")
                         .arg(parse_version.capturedTexts()[2])
                         .arg(parse_version.capturedTexts()[3]); //.join(" ");
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("This executable %1 may not be a valid X!Tandem software. "
                    "Please check your X!Tandem installation.")
          .arg(tandem_bin_path));
    }

  QProcess::ExitStatus Status = xt_process->exitStatus();
  delete xt_process;
  if(Status != 0)
    {
      // != QProcess::NormalExit
      throw pappso::PappsoException(
        QObject::tr("error executing X!Tandem Status != 0 : %1 %2\n%3")
          .arg(tandem_bin_path)
          .arg(arguments.join(" ").arg(result.data())));
    }
  qDebug();
  return version_return;
}


bool
TandemWrapperRun::shouldIstop()
{
  return false;
}


void
TandemWrapperRun::readyReadStandardOutput()
{
  *mp_outputStream << m_xtProcess->readAllStandardOutput();
  mp_outputStream->flush();
}

void
TandemWrapperRun::readyReadStandardError()
{
  *mp_errorStream << m_xtProcess->readAllStandardError();
  mp_errorStream->flush();
}

void
TandemWrapperRun::writeFinalTandemOutput(
  const QString &tmp_tandem_output,
  const QString &final_tandem_output,
  const QString &original_msdata_file_name)
{

  XtandemOutputSaxHandler wrap_output(final_tandem_output,
                                      original_msdata_file_name);

  wrap_output.setInputParameters("spectrum, timstof MS2 centroid parameters",
                                 m_centroidOptions);

  QFile qfile(tmp_tandem_output);
  QXmlInputSource xmlInputSource(&qfile);
  QXmlSimpleReader simplereader;
  simplereader.setContentHandler(&wrap_output);
  simplereader.setErrorHandler(&wrap_output);

  if(simplereader.parse(xmlInputSource))
    {
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("Error reading %1 X!Tandem output file :\n %2")
          .arg(tmp_tandem_output)
          .arg(wrap_output.errorString()));
    }
}

void
TandemWrapperRun::readTandemPresetFile(const QString &tandem_preset_file)
{
  // get number of threads and centroid parameters from tandem preset

  XtandemPresetSaxHandler preset_handler;

  QFile qfile(tandem_preset_file);
  QXmlInputSource xmlInputSource(&qfile);
  QXmlSimpleReader simplereader;
  simplereader.setContentHandler(&preset_handler);
  simplereader.setErrorHandler(&preset_handler);

  if(simplereader.parse(xmlInputSource))
    {

      uint ideal_number_of_thread = (uint)QThread::idealThreadCount();
      int cpu_number              = preset_handler.getNumberOfThreads();
      qDebug() << " cpu_number=" << cpu_number;
      // QThreadPool::globalInstance()->setMaxThreadCount(1);
      if(cpu_number > ideal_number_of_thread)
        {
          cpu_number = ideal_number_of_thread;
        }
      else
        {
          if(cpu_number > 0)
            {
              QThreadPool::globalInstance()->setMaxThreadCount(cpu_number);

              qDebug() << " maxThreadCount="
                       << QThreadPool::globalInstance()->maxThreadCount();
            }
        }

      if(!preset_handler.getCentroidOptions().isEmpty())
        {
          m_centroidOptions = preset_handler.getCentroidOptions();
        }
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("Error reading %1 X!Tandem preset file :\n %2")
          .arg(tandem_preset_file)
          .arg(preset_handler.errorString()));
    }
}


void
TandemWrapperRun::wrapTandemInputFile(const QString &tandem_input_file)
{
  // read original tandem input file
  // store original ms data file name
  // create new mzXML data file in temporary directory
  // create new tandem input file based on new mzXML file
  QString mzxml_data_file_name =
    mpa_temporaryDirectory->filePath("msdata.mzxml");
  QString wrapped_tandem_input =
    mpa_temporaryDirectory->filePath("input_tandem.xml");
  QString wrapped_tandem_output =
    mpa_temporaryDirectory->filePath("output_tandem.xml");

  XtandemInputSaxHandler wrap_input(
    mzxml_data_file_name, wrapped_tandem_input, wrapped_tandem_output);

  QFile qfile(tandem_input_file);
  QXmlInputSource xmlInputSource(&qfile);
  QXmlSimpleReader simplereader;
  simplereader.setContentHandler(&wrap_input);
  simplereader.setErrorHandler(&wrap_input);

  if(simplereader.parse(xmlInputSource))
    {
    }
  else
    {
      throw pappso::PappsoException(
        QObject::tr("Error reading %1 X!Tandem input file :\n %2")
          .arg(tandem_input_file)
          .arg(wrap_input.errorString()));
    }

  // get number of threads and centroid parameters from tandem preset
  readTandemPresetFile(wrap_input.getOriginalTandemPresetFileName());


  // convert to mzXML
  QString original_msdata_file_name = wrap_input.getOriginalMsDataFileName();
  convertOrginalMsData2mzXmlData(original_msdata_file_name,
                                 mzxml_data_file_name);


  // launch tandem
  runTandem(wrapped_tandem_input);

  // rewrite tandem result file
  writeFinalTandemOutput(wrapped_tandem_output,
                         wrap_input.getOriginalTandemOutputFileName(),
                         original_msdata_file_name);
}

void
TandemWrapperRun::convertOrginalMsData2mzXmlData(const QString &origin,
                                                 const QString &target) const
{
  qDebug();
  pappso::MsFileAccessor origin_access(origin, "runa1");
  origin_access.setPreferedFileReaderType(pappso::MzFormat::brukerTims,
                                          pappso::FileReaderType::tims_ms2);
  origin_access.getMsRunIds();

  pappso::MsRunReaderSPtr p_reader;
  p_reader = origin_access.msRunReaderSp(origin_access.getMsRunIds().front());
  if(!m_centroidOptions.isEmpty())
    {
      pappso::TimsMsRunReaderMs2 *tims2_reader =
        dynamic_cast<pappso::TimsMsRunReaderMs2 *>(p_reader.get());
      if(tims2_reader != nullptr)
        {
          qDebug();
          QStringList option_list = m_centroidOptions.split(" ");
          std::shared_ptr<pappso::FilterPseudoCentroid> ms2filter =
            std::make_shared<pappso::FilterPseudoCentroid>(
              option_list.at(0).toDouble(),
              option_list.at(1).toDouble(),
              option_list.at(2).toDouble(),
              option_list.at(3).toDouble());

          tims2_reader->setMs2FilterCstSPtr(ms2filter);
          qDebug();
        }
    }


  pappso::MzxmlOutput *p_mzxml_output;
  QFile output_file(target);
  // qDebug() << " TsvDirectoryWriter::writeSheet " <<
  // QFileInfo(*_p_ofile).absoluteFilePath();
  if(output_file.open(QIODevice::WriteOnly))
    {
      p_mzxml_output =
        new pappso::MzxmlOutput(QTextStream(&output_file).device());

      p_mzxml_output->maskMs1(true);

      p_mzxml_output->setReadAhead(true);

      p_mzxml_output->write(p_reader.get());

      p_mzxml_output->close();
    }
  else
    {
      throw pappso::PappsoException(
        tr("unable to write into %1 mzXML output file").arg(target));
    }

  qDebug();
}

void
TandemWrapperRun::run(const QString &tandem_input_file,
                      QTextStream &output_stream,
                      QTextStream &error_stream)
{
  mp_outputStream = &output_stream;
  mp_errorStream  = &error_stream;

  wrapTandemInputFile(tandem_input_file);
  mp_outputStream = nullptr;
  mp_errorStream  = nullptr;
}

void
TandemWrapperRun::runTandem(const QString &tandem_input_file)
{

  m_xtProcess = new QProcess();
  QStringList arguments;

  qDebug() << m_tandemBinary << " " << m_xtProcess->arguments();

  arguments << tandem_input_file;
  // hk_process->setWorkingDirectory(QFileInfo(_hardklor_exe).absolutePath());
  m_xtProcess->start(m_tandemBinary, arguments);

  qDebug() << m_tandemBinary << " " << m_xtProcess->arguments();

  connect(m_xtProcess,
          &QProcess::readyReadStandardOutput,
          this,
          &TandemWrapperRun::readyReadStandardOutput);
  connect(m_xtProcess,
          &QProcess::readyReadStandardError,
          this,
          &TandemWrapperRun::readyReadStandardError);


  qDebug() << m_tandemBinary << " " << m_xtProcess->arguments();

  if(!m_xtProcess->waitForStarted())
    {
      throw pappso::PappsoException(
        QObject::tr("X!Tandem process failed to start"));
    }

  qDebug() << m_tandemBinary << " " << m_xtProcess->arguments();

  while(m_xtProcess->waitForFinished(m_maxTandemRunTimeMs))
    {
      //_p_monitor->appendText(xt_process->readAll().data());
      // data.append(xt_process->readAll());
      if(shouldIstop())
        {
          m_xtProcess->kill();
          delete m_xtProcess;
          throw pappso::PappsoException(
            QObject::tr("X!Tandem stopped by the user processing on file %1")
              .arg(tandem_input_file));
        }
    }

  QProcess::ExitStatus Status = m_xtProcess->exitStatus();

  delete m_xtProcess;
  if(Status != 0)
    {
      // != QProcess::NormalExit
      throw pappso::PappsoException(
        QObject::tr("error executing X!Tandem Status != 0 : %1")
          .arg(m_tandemBinary));
    }
  m_xtProcess = nullptr;
}
