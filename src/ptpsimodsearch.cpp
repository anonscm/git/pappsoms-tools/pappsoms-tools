
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ptpsimodsearch.h"


#include <QCoreApplication>
#include <QTimer>
#include <QDebug>
#include <QDateTime>
#include <QDir>
#include <QCommandLineParser>
#include <pappsomspp/precision.h>
#include <pappsomspp/obo/filterobopsimodtermlabel.h>
#include <pappsomspp/obo/filterobopsimodsink.h>
#include <pappsomspp/obo/filterobopsimodtermdiffmono.h>
#include <pappsomspp/obo/filterobopsimodtermlabel.h>
#include <pappsomspp/obo/filterobopsimodtermname.h>
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvdirectorywriter.h>
#include <odsstream/tsvoutputstream.h>


using namespace pappso;


void
PtPsiModSearch::writePeptideIonTable(CalcWriterInterface &writer,
                                     const std::list<OboPsiModTerm> &term_list)
{
  // unsigned int zmax = 4;
  // DaltonPrecision precision(0.5);
  // std::list<PeptideIon> ion_list =
  // PeptideFragmentIonListBase::getCIDionList();

  OdsTableCellStyle styleHeader;
  styleHeader.setBackgroundColor(QColor("grey"));

  OdsTableCellStyleRef styleHeaderRef =
    writer.getTableCellStyleRef(styleHeader);

  writer.writeSheet("PTM list");


  for(auto psi_mod_term : term_list)
    {
      writer.setTableCellStyleRef(styleHeaderRef);
      writer.writeCell("accession");
      writer.clearTableCellStyleRef();
      writer.writeCell(psi_mod_term.m_accession);
      writer.writeLine();


      writer.setTableCellStyleRef(styleHeaderRef);
      writer.writeCell("name");
      writer.clearTableCellStyleRef();
      writer.writeCell(psi_mod_term.m_name);
      writer.writeLine();

      writer.setTableCellStyleRef(styleHeaderRef);
      writer.writeCell("PSI-MS label");
      writer.clearTableCellStyleRef();
      writer.writeCell(psi_mod_term.m_psiMsLabel);
      writer.writeLine();

      writer.setTableCellStyleRef(styleHeaderRef);
      writer.writeCell("PSI-MOD label");
      writer.clearTableCellStyleRef();
      writer.writeCell(psi_mod_term.m_psiModLabel);
      writer.writeLine();

      writer.setTableCellStyleRef(styleHeaderRef);
      writer.writeCell("diff mono");
      writer.clearTableCellStyleRef();
      writer.writeCell(psi_mod_term.m_diffMono);
      writer.writeLine();

      writer.setTableCellStyleRef(styleHeaderRef);
      writer.writeCell("diff formula");
      writer.clearTableCellStyleRef();
      writer.writeCell(psi_mod_term.m_diffFormula);
      writer.writeLine();

      writer.writeLine();
    }
}

PtPsiModSearch::PtPsiModSearch(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}


// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
PtPsiModSearch::run()
{
  //./src/pt-psimodsearch --label Carbamidomethyl
  //./src/pt-psimodsearch --mass 79.02 --ppm 0.2
  //./src/pt-psimodsearch --mass 79.02 --dalton 0.2
  //./src/pt-psimodsearch --acc MOD:00397
  //./src/pt-psimodsearch --name oxydation


  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qDebug() << "PtPeptideMass::run() begin";
      QCommandLineParser parser;

      parser.setApplicationDescription(
        QString(SOFTWARE_NAME)
          .append(" ")
          .append(PAPPSOMSTOOLS_VERSION)
          .append(" search PTM in the PSI-MOD ontology.\nSearch for post "
                  "translational modifications (PTMs) with a simple query "
                  "based on mutliple criterium (mass difference, label, "
                  "name...). The complete list of PTMs is based on the PSI-MOD "
                  "ontology (OBO format)."));
      parser.addHelpOption();
      parser.addVersionOption();
      // parser.addPositionalArgument("peptide",
      // QCoreApplication::translate("main", "peptide sequence"));
      QCommandLineOption massOption(
        QStringList() << "m"
                      << "mass",
        QCoreApplication::translate("main", "PTM mass to look for (Daltons)"),
        QCoreApplication::translate("main", "mass"));
      QCommandLineOption daltonOption(
        QStringList() << "d"
                      << "dalton",
        QCoreApplication::translate(
          "main", "Dalton precision mass range (+-0.1 by default)"),
        QCoreApplication::translate("main", "dalton"), QString());
      QCommandLineOption labelOption(
        QStringList() << "l"
                      << "label",
        QCoreApplication::translate("main", "PSI MOD or PSI MS label search"),
        QCoreApplication::translate("main", "label"));

      QCommandLineOption nameOption(
        QStringList() << "n"
                      << "name",
        QCoreApplication::translate("main", "PSI MOD search by name"),
        QCoreApplication::translate("main", "name"));
      QCommandLineOption odsOption(
        QStringList() << "o"
                      << "ods",
        QCoreApplication::translate("main",
                                    "Write results in an ODS file <output>."),
        QCoreApplication::translate("main", "output"));
      QCommandLineOption tsvOption(
        QStringList() << "t"
                      << "tsv",
        QCoreApplication::translate(
          "main", "Write results in TSV files contained in a <directory>."),
        QCoreApplication::translate("main", "directory"), QString());
      parser.addOption(odsOption);
      parser.addOption(tsvOption);
      parser.addOption(massOption);
      parser.addOption(daltonOption);
      parser.addOption(labelOption);
      parser.addOption(nameOption);


      qDebug() << "PtPeptideMass::run() 1";

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << "PtPeptideMass.Run is executing";

      const QDateTime dt_begin = QDateTime::currentDateTime();

      PrecisionPtr p_precision = PrecisionFactory::getDaltonInstance(0.1);
      if(!parser.value(daltonOption).isEmpty())
        {
          pappso_double dalton_number =
            (pappso_double)parser.value(daltonOption).toDouble();
          delete p_precision;
          p_precision = PrecisionFactory::getDaltonInstance(dalton_number);
        }

      FilterOboPsiModSink term_list;

      OboPsiModHandlerInterface *last_men_standing = &term_list;
      FilterOboPsiModTermDiffMono *p_filter_mass   = nullptr;
      FilterOboPsiModTermLabel *p_filter_label     = nullptr;
      FilterOboPsiModTermName *p_filter_name       = nullptr;

      if(!parser.value(massOption).isEmpty())
        {
          qDebug() << " if (!parser.value(massOption).isEmpty())"
                   << parser.value(massOption);
          pappso_double mass_search = parser.value(massOption).toDouble();
          p_filter_mass             = new FilterOboPsiModTermDiffMono(
            *last_men_standing, MzRange(mass_search, p_precision));

          last_men_standing = p_filter_mass;
        }
      if(!parser.value(labelOption).isEmpty())
        {
          qDebug() << " if (!parser.value(labelOption).isEmpty())"
                   << parser.value(labelOption);
          p_filter_label = new FilterOboPsiModTermLabel(
            *last_men_standing, parser.value(labelOption));

          last_men_standing = p_filter_label;
        }
      if(!parser.value(nameOption).isEmpty())
        {
          qDebug() << " if (!parser.value(nameOption).isEmpty())"
                   << parser.value(nameOption);
          p_filter_name = new FilterOboPsiModTermName(
            *last_men_standing, QString("*%1*").arg(parser.value(nameOption)));

          last_men_standing = p_filter_name;
        }

      OboPsiMod psimodb(*last_men_standing);

      if(p_filter_mass != nullptr)
        {
          delete(p_filter_mass);
        }
      if(p_filter_label != nullptr)
        {
          delete(p_filter_label);
        }
      if(p_filter_name != nullptr)
        {
          delete(p_filter_name);
        }


      QTextStream outputStream(stdout, QIODevice::WriteOnly);
      outputStream.setRealNumberPrecision(10);
      TsvOutputStream writer(outputStream);


      writePeptideIonTable(writer, term_list.getOboPsiModTermList());

      QString odsFileStr = parser.value(odsOption);
      if(!odsFileStr.isEmpty())
        {
          QFile file(odsFileStr);
          // file.open(QIODevice::WriteOnly);
          OdsDocWriter writer(&file);
          // quantificator.report(writer);
          writePeptideIonTable(writer, term_list.getOboPsiModTermList());
          writer.close();
          file.close();
        }
      QString tsvFileStr = parser.value(tsvOption);
      if(!tsvFileStr.isEmpty())
        {
          QDir directory(tsvFileStr);
          qDebug() << "QDir directory(tsvFileStr) " << directory.absolutePath()
                   << " " << tsvFileStr;
          // file.open(QIODevice::WriteOnly);
          TsvDirectoryWriter writer(directory);
          writePeptideIonTable(writer, term_list.getOboPsiModTermList());
          // quantificator.report(writer);
          writer.close();
        }

      // readMzXml();
      qDebug() << "MasschroqPrmCli::run() end";
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.what() << endl;
      exit(1);
      app->exit(1);
    }

  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
PtPsiModSearch::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
PtPsiModSearch::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{

  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << "main begin";
  QCoreApplication app(argc, argv);
  qDebug() << "main 1";
  QCoreApplication::setApplicationName("pt-psimodsearch");
  QCoreApplication::setApplicationVersion(PAPPSOMSTOOLS_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  PtPsiModSearch myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(&app, SIGNAL(aboutToQuit()), &myMain,
                   SLOT(aboutToQuitApp()));
  qDebug() << "main 2";


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
