
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "pt_peptidemass.h"

#include <iostream>
#include <QCoreApplication>
#include <QTimer>
#include <QDebug>
#include <QDateTime>
#include <QDir>
#include <QCommandLineParser>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/peptide/peptidenaturalisotopelist.h>
#include <pappsomspp/peptide/peptidestrparser.h>
#include <pappsomspp/pappsoexception.h>
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvdirectorywriter.h>
#include <odsstream/tsvoutputstream.h>
#include <pappsomspp/pappsoexception.h>
#include <vector>

using namespace pappso;


void
PtPeptideMass::writePeptideMass(CalcWriterInterface &writer,
                                const pappso::PeptideSp &peptideSp)
{
  QColor red("black");
  OdsTableCellStyle style;
  style.setBackgroundColor(QColor("lightGrey"));
  style.setTextColor(red);

  OdsTableCellStyleRef style_title = writer.getTableCellStyleRef(style);


  writer.writeSheet("peptide");
  writer.writeCell("sequence");
  writer.writeCell(peptideSp.get()->getSequence());
  writer.writeLine();
  writer.writeCell("formula");
  writer.writeCell(peptideSp.get()->getFormula(0));
  writer.writeLine();
  writer.writeCell("mass");
  writer.writeCell(peptideSp.get()->getMass());
  writer.writeLine();
  writer.writeCell("mz (z=1)");
  writer.writeCell(peptideSp.get()->getMz(1));
  writer.writeLine();
  writer.writeCell("mz (z=2)");
  writer.writeCell(peptideSp.get()->getMz(2));
  writer.writeLine();
  writer.writeCell("mz (z=3)");
  writer.writeCell(peptideSp.get()->getMz(3));


  PeptideNaturalIsotopeList isotopeList(peptideSp);
  std::map<unsigned int, pappso_double> map_isotope_number =
    isotopeList.getIntensityRatioPerIsotopeNumber();


  writer.setTableCellStyleRef(style_title);
  writer.writeSheet("isotopes");
  writer.writeCell("number");
  writer.writeCell("ratio");
  writer.writeCell("m/z (z=1)");
  writer.writeLine();
  writer.clearTableCellStyleRef();

  PrecisionPtr precision = PrecisionFactory::getDaltonInstance(0.3);
  for(unsigned int i = 0; i < map_isotope_number.size(); i++)
    {
      writer.writeCell(i);
      writer.writeCell(map_isotope_number[i]);

      PeptideNaturalIsotopeAverage isotopeAverageMono(
        isotopeList, 1, i, 1, precision);
      writer.writeCell(isotopeAverageMono.getMz());
      writer.writeLine();
    }


  writer.writeSheet("complete isotope list");


  writer.writeCell(peptideSp.get()->getSequence());
  writer.writeLine();

  writer.setTableCellStyleRef(style_title);
  writer.writeCell("isotope number");
  writer.writeCell("formula (z=1)");
  writer.writeCell("mz (z=1)");
  writer.writeCell("ratio (z=1)");
  writer.writeLine();
  writer.clearTableCellStyleRef();

  for(int i = 0; i < 6; i++)
    {
      auto isotopeLevelList = isotopeList.getByIsotopeNumber(i, 1);
      auto it               = isotopeLevelList.begin();
      while(it != isotopeLevelList.end())
        {
          writer.writeCell(it->get()->getIsotopeNumber());
          writer.writeCell(it->get()->getFormula(1));
          writer.writeCell(it->get()->getMz(1));
          writer.writeCell(it->get()->getIntensityRatio(1));
          writer.writeLine();
          // qDebug() <<  it->getIntensityRatio();
          it++;
        }
    }
}

PtPeptideMass::PtPeptideMass(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}


// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
PtPeptideMass::run()
{
  //./src/masschroqprm -p ../doc/xml/peptide_list_bsa.xml -o peptide_bsa.ods
  /// gorgone/pappso/moulon/raw/test_PRM/bsa_prm_01.mzXML

  //./src/masschroqprm -p
  /// gorgone/pappso/moulon/users/Olivier/masschroqprm/bsa_prm01/xtp334/input_mcqprm.xml
  /// gorgone/pappso/moulon/raw/test_PRM/bsa_prm_01.mzXML

  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qDebug() << "PtPeptideMass::run() begin";
      QCommandLineParser parser;

      parser.setApplicationDescription(QString(SOFTWARE_NAME)
                                         .append(" ")
                                         .append(PAPPSOMSTOOLS_VERSION)
                                         .append(" peptide mass calculator"));
      parser.addHelpOption();
      parser.addVersionOption();
      parser.addPositionalArgument(
        "peptide", QCoreApplication::translate("main", "peptide sequence"));
      QCommandLineOption odsOption(
        QStringList() << "o"
                      << "ods",
        QCoreApplication::translate("main",
                                    "Write results in an ODS file <output>."),
        QCoreApplication::translate("main", "output"));
      QCommandLineOption tsvOption(
        QStringList() << "t"
                      << "tsv",
        QCoreApplication::translate(
          "main", "Write results in TSV files contained in a <directory>."),
        QCoreApplication::translate("main", "directory"),
        QString());
      parser.addOption(odsOption);
      parser.addOption(tsvOption);

      qDebug() << "PtPeptideMass::run() 1";

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << "PtPeptideMass.Run is executing";

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QFileInfo masschroqprm_dir_path(
        QCoreApplication::applicationDirPath());
      const QStringList args = parser.positionalArguments();
      QString peptideStr(args.at(0));

      PeptideSp peptideSp = PeptideStrParser::parseString(peptideStr);

      QTextStream outputStream(stdout, QIODevice::WriteOnly);
      outputStream.setRealNumberPrecision(10);
      TsvOutputStream writer(outputStream);
      writePeptideMass(writer, peptideSp);

      QString odsFileStr = parser.value(odsOption);
      if(!odsFileStr.isEmpty())
        {
          QFile file(odsFileStr);
          // file.open(QIODevice::WriteOnly);
          OdsDocWriter writer(&file);
          // quantificator.report(writer);
          writePeptideMass(writer, peptideSp);
          writer.close();
          file.close();
        }
      QString tsvFileStr = parser.value(tsvOption);
      if(!tsvFileStr.isEmpty())
        {
          QDir directory(tsvFileStr);
          qDebug() << "QDir directory(tsvFileStr) " << directory.absolutePath()
                   << " " << tsvFileStr;
          // file.open(QIODevice::WriteOnly);
          TsvDirectoryWriter writer(directory);
          writePeptideMass(writer, peptideSp);
          // quantificator.report(writer);
          writer.close();
        }

      // readMzXml();
      qDebug() << "MasschroqPrmCli::run() end";
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.what() << endl;
      exit(1);
      app->exit(1);
    }

  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
PtPeptideMass::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
PtPeptideMass::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{

  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << "main begin";
  QCoreApplication app(argc, argv);
  qDebug() << "main 1";
  QCoreApplication::setApplicationName("pt-peptidemass");
  QCoreApplication::setApplicationVersion(PAPPSOMSTOOLS_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  PtPeptideMass myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug() << "main 2";


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
