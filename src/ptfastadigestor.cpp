
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ptfastadigestor.h"


#include <iostream>
#include <QCoreApplication>
#include <QTimer>
#include <QDebug>
#include <QDateTime>
#include <QDir>
#include <QCommandLineParser>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/fasta/fastareader.h>
#include <pappsomspp/protein/protein.h>
#include <pappsomspp/protein/peptidesizefilter.h>
#include <pappsomspp/protein/enzyme.h>
#include <pappsomspp/protein/peptidebuilder.h>
#include <pappsomspp/protein/peptidevariablemodificationbuilder.h>
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvdirectorywriter.h>
#include <odsstream/tsvoutputstream.h>

#include <vector>

using namespace pappso;


PtFastaDigestor::PtFastaDigestor(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}


class PeptideModCountHandler : public PeptideModificatorInterface
{
  public:
  PeptideModCountHandler(MzRange &mass_range) : _mass_range(mass_range){};
  ~PeptideModCountHandler()
  {
    qDebug() << "~PeptideModCountHandler " << _count_target;
  };

  void
  setPeptideSp(std::int8_t sequence_database_id, const ProteinSp &protein_sp,
               bool is_decoy, const PeptideSp &peptide_sp, unsigned int start,
               bool is_nter, unsigned int missed_cleavage_number,
               bool semi_enzyme) override
  {
    // qDebug() << start << " " << peptide_sp.get()->toString();
    // if (_mass_range.contains(peptide_sp.get()->getMz(1))) _count_target++;
    if(_mass_range.contains(peptide_sp.get()->getMz(2)))
      {
        _count_target++;
        qDebug() << protein_sp.get()->getAccession() << " " << start << " "
                 << peptide_sp.get()->toAbsoluteString() << " "
                 << peptide_sp.get()->getSequence();
      }
    // if (_mass_range.contains(peptide_sp.get()->getMz(3))) _count_target++;

    //=> produce fake peptides with semi tryptic cleavage
    //=> omit Cter and Nter oxydation
    //=> rotate sequece
  };

  private:
  MzRange _mass_range;

  unsigned long _count_target = 0;
};


class PeptideListHandler : public EnzymeProductInterface
{
  public:
  void
  setPeptide(std::int8_t sequence_database_id, const ProteinSp &protein_sp,
             bool is_decoy, const QString &peptide, unsigned int start,
             bool is_nter, unsigned int missed_cleavage_number,
             bool semi_enzyme) override
  {
    qDebug() << start << " " << peptide;
    _peptide_list << peptide;
  };

  QStringList _peptide_list;
};


class ProteinDigestorReader : public FastaHandlerInterface
{
  public:
  ProteinDigestorReader(QTextStream &otxtstream,
                        PeptideListHandler &peptide_output,
                        unsigned int min_size, unsigned int max_size,
                        unsigned int miscleavage)
    : _peptide_output(peptide_output)
  {
    _trypsin.setMiscleavage(miscleavage);

    _p_peptide_size2varmod = new PeptideSizeFilter(min_size, max_size);
    _p_peptide_size2varmod->setSink(&_peptide_output);
    //_peptide_x_wildcard.setSink(&_peptide_output);
    _p_outStream = &otxtstream; // new QTextStream(stdout,
                                // QIODevice::WriteOnly);
  };
  ~ProteinDigestorReader()
  {
    // delete (_p_peptide_size2varmod);
  }

  void
  setSequence(const QString &description, const QString &sequence) override
  {
    qDebug() << "ProteinReader::setSequence " << description << " " << sequence;
    QString new_seq = sequence;
    ProteinSp protein_sp =
      Protein(description, new_seq.replace(QRegExp("\\*$"), ""))
        .removeTranslationStop()
        .makeProteinSp();

    _trypsin.eat(0, protein_sp, false, *_p_peptide_size2varmod);

    *_p_outStream << ">" << description << "\n";
    *_p_outStream << _peptide_output._peptide_list.join(" ") << "\n";

    _peptide_output._peptide_list.clear();
  };

  private:
  Enzyme _trypsin;
  PeptideSizeFilter *_p_peptide_size2varmod;
  // PeptideXwildcard _peptide_x_wildcard;
  PeptideListHandler &_peptide_output;
  QTextStream *_p_outStream;
};

class ProteinReader : public FastaHandlerInterface
{
  public:
  ProteinReader(PeptideModificatorInterface &peptide_output,
                unsigned int min_size, unsigned int max_size,
                unsigned int miscleavage)
    : _peptide_output(peptide_output)
  {
    _trypsin.setMiscleavage(miscleavage);
    _carbamido = AaModification::getInstance("MOD:00397");
    _met_oxy   = AaModification::getInstance("MOD:00719");

    _p_var_mod_builder = new PeptideVariableModificationBuilder(_met_oxy);
    _p_var_mod_builder->addAa('M');
    _p_var_mod_builder->setSink(&_peptide_output);
    _p_fixed_mod_builder = new PeptideBuilder;
    _p_fixed_mod_builder->setSink(_p_var_mod_builder);
    _p_fixed_mod_builder->addFixedAaModification('C', _carbamido);
    _p_peptide_size2varmod = new PeptideSizeFilter(min_size, max_size);
    _p_peptide_size2varmod->setSink(_p_fixed_mod_builder);
    //_peptide_x_wildcard.setSink(_p_fixed_mod_builder);
  };
  ~ProteinReader()
  {
    delete(_p_fixed_mod_builder);
    delete(_p_peptide_size2varmod);
    delete(_p_var_mod_builder);
  }

  void
  setSequence(const QString &description, const QString &sequence) override
  {
    // qDebug() << "ProteinReader::setSequence " << description << " "
    // <<sequence;
    QString new_seq = sequence;
    ProteinSp protein_sp =
      Protein(description, new_seq.replace(QRegExp("\\*$"), ""))
        .makeProteinSp();

    _trypsin.eat(0, protein_sp, false, *_p_peptide_size2varmod);
  };

  private:
  Enzyme _trypsin;
  AaModificationP _carbamido;
  AaModificationP _met_oxy;
  PeptideModificatorInterface &_peptide_output;
  PeptideVariableModificationBuilder *_p_var_mod_builder;
  PeptideBuilder *_p_fixed_mod_builder;
  PeptideSizeFilter *_p_peptide_size2varmod;
};

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
PtFastaDigestor::run()
{
  // ./src/pt-fastagrouper ../../pappsomspp/test/data/asr1_digested_peptides.txt
  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qDebug() << "PtFastaDigestor::run() begin";
      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(QString(SOFTWARE_NAME)
                                         .append(" ")
                                         .append(PAPPSOMSTOOLS_VERSION)
                                         .append(" FASTA grouper"));
      parser.addHelpOption();
      parser.addVersionOption();
      QCommandLineOption inputOption(
        QStringList() << "i"
                      << "fasta",
        QCoreApplication::translate("main", "FASTA file <input>."),
        QCoreApplication::translate("main", "input"));

      QCommandLineOption outputOption(
        QStringList() << "f",
        QCoreApplication::translate("main", "FASTA file <output>."),
        QCoreApplication::translate("main", "output"));
      QCommandLineOption misOption(
        QStringList() << "m"
                      << "miss",
        QCoreApplication::translate(
          "main", "number of miscleavage allowed (default:0)"),
        QCoreApplication::translate("main", "miscleavage"), "0");
      QCommandLineOption minOption(
        QStringList() << "min",
        QCoreApplication::translate(
          "main", "minimal length of peptides to generate (default:7)"),
        QCoreApplication::translate("main", "min"), "7");
      QCommandLineOption maxOption(
        QStringList() << "max",
        QCoreApplication::translate(
          "main", "maximal length of peptides to generate (default:35)"),
        QCoreApplication::translate("main", "max"), "35");
      parser.addOption(outputOption);
      parser.addOption(inputOption);
      parser.addOption(misOption);
      parser.addOption(minOption);
      parser.addOption(maxOption);

      qDebug() << "PtFastaDigestor::run() 1";

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << "PtFastaDigestor.Run is executing";

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QFileInfo masschroqprm_dir_path(
        QCoreApplication::applicationDirPath());
      const QStringList args = parser.positionalArguments();

      QString fastaFileStr = parser.value(inputOption);
      QFile fastaFile;
      if(fastaFileStr.isEmpty())
        {
          fastaFile.open(stdin, QIODevice::ReadOnly);
        }
      else
        {
          fastaFile.setFileName(fastaFileStr);
          fastaFile.open(QIODevice::ReadOnly);
        }

      QString fileStr = parser.value(outputOption);
      QTextStream *p_outputStream;
      QFile outFile;
      if(fileStr.isEmpty())
        {
          p_outputStream = new QTextStream(stdout, QIODevice::WriteOnly);
        }
      else
        {
          outFile.setFileName(fileStr);
          outFile.open(QIODevice::WriteOnly);
          p_outputStream = new QTextStream(&outFile);
        }

      PeptideListHandler peptide_output;
      ProteinDigestorReader protein_reader(
        *p_outputStream, peptide_output, parser.value(minOption).toUInt(),
        parser.value(maxOption).toUInt(), parser.value(misOption).toUInt());
      FastaReader reader(protein_reader);
      reader.parse(&fastaFile);
      fastaFile.close();

      p_outputStream->flush();
      delete p_outputStream;


      if(fastaFileStr.isEmpty())
        {
          fastaFile.open(stdin, QIODevice::ReadOnly);
        }
      else
        {
          fastaFile.setFileName(fastaFileStr);
          fastaFile.open(QIODevice::ReadOnly);
        }
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.what() << endl;
      exit(1);
      app->exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
PtFastaDigestor::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
PtFastaDigestor::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << "main begin";
  QCoreApplication app(argc, argv);
  qDebug() << "main 1";
  QCoreApplication::setApplicationName("pt-fastadigector");
  QCoreApplication::setApplicationVersion(PAPPSOMSTOOLS_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  PtFastaDigestor myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(&app, SIGNAL(aboutToQuit()), &myMain,
                   SLOT(aboutToQuitApp()));
  qDebug() << "main 2";


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
