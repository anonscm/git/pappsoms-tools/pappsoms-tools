
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ptfastatrypticpeptidecount.h"
#include <QCommandLineParser>
#include <QDateTime>
#include <QTimer>
#include <QFile>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/fasta/fastareader.h>
#include <pappsomspp/protein/protein.h>
#include <pappsomspp/protein/enzyme.h>
#include <odsstream/odsdocwriter.h>
#include <odsstream/odsexception.h>


class DigestionHandler : public pappso::EnzymeProductInterface
{
  public:
  void
  setPeptide(std::int8_t sequence_database_id,
             const pappso::ProteinSp &protein_sp,
             bool is_decoy,
             const QString &peptide,
             unsigned int start,
             bool is_nter,
             unsigned int missed_cleavage_number,
             bool semi_enzyme) override
  {
    _peptide_list.append(peptide);
  };

  QStringList _peptide_list;
};

class FastaPeptideCount : public pappso::FastaHandlerInterface
{
  public:
  FastaPeptideCount(CalcWriterInterface &writer) : _writer(writer)
  {


    qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
    _writer.writeSheet("tryptic peptide count");
  };

  void
  setSequence(const QString &description, const QString &sequence) override
  {
    qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
    pappso::ProteinSp protein =
      std::make_shared<const pappso::Protein>(description, sequence);

    _writer.writeCell(protein->getAccession());


    // qDebug() << "ProteinXtp::countTrypticPeptidesForPAI begin";
    try
      {
        pappso::Enzyme kinase;
        kinase.setMiscleavage(0);
        kinase.setTakeOnlyFirstWildcard(true);
        DigestionHandler digestion;

        kinase.eat(0, protein, false, digestion);

        unsigned int count = 0;
        for(const QString &peptide_str : digestion._peptide_list)
          {
            pappso::Peptide peptide(peptide_str);
            pappso::pappso_double mass = peptide.getMass();
            if((mass > 800) && (mass < 2500))
              {
                count++;
              }
          }

        // qDebug() << "ProteinXtp::countTrypticPeptidesForPAI end";
        _writer.writeCell(count);
      }
    catch(pappso::PappsoException error)
      {
        throw pappso::PappsoException(
          QObject::tr(
            "Error in countTrypticPeptidesForPAI for protein %1 :\n%2")
            .arg(protein->getAccession())
            .arg(error.qwhat()));
      }


    _writer.writeLine();
    qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  };

  private:
  CalcWriterInterface &_writer;
};


PtFastaTrypticPeptideCount::PtFastaTrypticPeptideCount(QObject *parent)
  : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
PtFastaTrypticPeptideCount::run()
{
  // ./src/pt-fastagrouper ../../pappsomspp/test/data/asr1_digested_peptides.txt


  //./src/pt-fastarenamer -i
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_ortho_uniprot_TK24_M145.fasta
  //-c
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_TK24_M145.ods
  //-o /tmp/test.fasta

  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(QString(SOFTWARE_NAME)
                                         .append(" ")
                                         .append(PAPPSOMSTOOLS_VERSION)
                                         .append(" FASTA renamer"));
      parser.addHelpOption();
      parser.addVersionOption();
      QCommandLineOption inputOption(
        QStringList() << "i"
                      << "fasta",
        QCoreApplication::translate("main", "FASTA file <input>."),
        QCoreApplication::translate("main", "input"));

      QCommandLineOption outputOption(
        QStringList() << "o"
                      << "output",
        QCoreApplication::translate("main", "Write ODS output file <output>."),
        QCoreApplication::translate("main", "output"));
      parser.addOption(outputOption);
      parser.addOption(inputOption);

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QStringList args   = parser.positionalArguments();


      QString fastaFileStr = parser.value(inputOption);
      QFile fastaFile;
      if(fastaFileStr.isEmpty())
        {
          fastaFile.open(stdin, QIODevice::ReadOnly);
        }
      else
        {
          fastaFile.setFileName(fastaFileStr);
          fastaFile.open(QIODevice::ReadOnly);
        }


      QString odsFileStr = parser.value(outputOption);
      if(!odsFileStr.isEmpty())
        {
          QFile file(odsFileStr);
          // file.open(QIODevice::WriteOnly);
          OdsDocWriter writer(&file);


          FastaPeptideCount peptide_count(writer);
          pappso::FastaReader reader(peptide_count);
          reader.parse(&fastaFile);
          fastaFile.close();


          writer.close();
          file.close();
        }
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
    }
  catch(OdsException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools, reading an "
                     "ODS file. Dont Panic :"
                  << endl;
      errorStream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.what() << endl;
      exit(1);
      app->exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
PtFastaTrypticPeptideCount::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
PtFastaTrypticPeptideCount::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QCoreApplication app(argc, argv);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QCoreApplication::setApplicationName("pt-fastatrypticpeptidecount");
  QCoreApplication::setApplicationVersion(PAPPSOMSTOOLS_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  PtFastaTrypticPeptideCount myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
