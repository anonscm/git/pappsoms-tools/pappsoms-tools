
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "ptfastarenamer.h"
#include <QCommandLineParser>
#include <QDateTime>
#include <QTimer>
#include <QFile>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/fasta/fastareader.h>
#include <pappsomspp/fasta/fastaoutputstream.h>
#include <odsstream/odsdocreader.h>
#include <odsstream/odsexception.h>
#include "utils/mapods.h"


using namespace pappso;

class FastaRenamer : public FastaHandlerInterface
{
  public:
  FastaRenamer(FastaWriterInterface &output,
               std::map<QString, std::vector<QString>> correspondance_table,
               bool include_all)
    : _output(output)
  {

    _include_all = include_all;
    if(correspondance_table.find("old accession") == correspondance_table.end())
      {
        throw PappsoException(
          QObject::tr("the correspondande table MUST contain a column named "
                      "\"old accession\""));
      }

    _old_accession_list = correspondance_table.at("old accession");
    if(correspondance_table.find("new accession") != correspondance_table.end())
      {
        _new_accession_list = correspondance_table.at("new accession");
        if(_old_accession_list.size() != _new_accession_list.size())
          {
            throw PappsoException(
              QObject::tr(
                "%1 column and %2 column must contain the same number of items")
                .arg("old accession")
                .arg("new accession"));
          }
      }
    if(correspondance_table.find("new description") !=
       correspondance_table.end())
      {
        _new_description_list = correspondance_table.at("new description");
        if(_old_accession_list.size() != _new_description_list.size())
          {
            throw PappsoException(
              QObject::tr(
                "%1 column and %2 column must contain the same number of items")
                .arg("old accession")
                .arg("new description"));
          }
      }

    qDebug() << "FastaRenamer _old_accession_list.size() "
             << _old_accession_list.size() << " _new_accession_list.size()"
             << _new_accession_list.size() << " _new_description_list.size()"
             << _new_description_list.size();
  };

  void
  setSequence(const QString &description, const QString &sequence) override
  {
    qDebug() << "FastaRenamer::setSequence " << description << " " << sequence;
    Protein protein(description, sequence);

    qDebug() << "FastaRenamer::setSequence getAccession";
    QString old_accession = protein.getAccession();

    unsigned int pos = 0;

    qDebug() << "FastaRenamer::setSequence pos";
    while((pos < _old_accession_list.size()) &&
          (_old_accession_list[pos] != old_accession))
      {
        // qDebug() << pos << " " << _old_accession_list[pos] << old_accession;
        pos++;
      }
    if(pos < _old_accession_list.size())
      {
        // qDebug() << "found |"  << old_accession<<"|" << pos;
        if(_new_accession_list.size() > 0)
          {
            protein.setAccession(_new_accession_list[pos]);
          }

        if(_new_description_list.size() > 0)
          {
            protein.setDescription(_new_description_list[pos]);
          }
        _output.writeProtein(protein);
      }
    else
      {

        // qDebug() << "NOT found |"  << old_accession<< "|" << pos;
        if(_include_all)
          {
            _output.writeProtein(protein);
          }
      }
    qDebug() << "FastaRenamer::setSequence " << description << " "
             << " end";
  };

  private:
  std::vector<QString> _old_accession_list;
  std::vector<QString> _new_accession_list;
  std::vector<QString> _new_description_list;
  FastaWriterInterface &_output;
  bool _include_all;
};


PtFastaRenamer::PtFastaRenamer(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
PtFastaRenamer::run()
{
  // ./src/pt-fastagrouper ../../pappsomspp/test/data/asr1_digested_peptides.txt


  //./src/pt-fastarenamer -i
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_ortho_uniprot_TK24_M145.fasta
  //-c
  /// gorgone/pappso/jouy/database/Strepto_Aaron/20151106_grouping_TK24_M145.ods
  //-o /tmp/test.fasta

  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qDebug() << "PtFastaRenamer::run() begin";
      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(QString(SOFTWARE_NAME)
                                         .append(" ")
                                         .append(PAPPSOMSTOOLS_VERSION)
                                         .append(" FASTA renamer"));
      parser.addHelpOption();
      parser.addVersionOption();
      QCommandLineOption inputOption(
        QStringList() << "i"
                      << "fasta",
        QCoreApplication::translate("main", "FASTA file to rename <input>."),
        QCoreApplication::translate("main", "input"));
      QCommandLineOption includeOption(
        QStringList() << "a"
                      << "all",
        QCoreApplication::translate("main",
                                    "include all sequences in the result file, "
                                    "even if there is no correspondance."));

      QCommandLineOption correspOption(
        QStringList() << "c"
                      << "corresp",
        QCoreApplication::translate(
          "main", "correspondance table of accessions <ods>."),
        QCoreApplication::translate("main", "ods"));

      QCommandLineOption outputOption(
        QStringList() << "o"
                      << "output",
        QCoreApplication::translate("main",
                                    "Write renamed FASTA file <output>."),
        QCoreApplication::translate("main", "output"));
      parser.addOption(correspOption);
      parser.addOption(outputOption);
      parser.addOption(inputOption);
      parser.addOption(includeOption);

      qDebug() << "PtFastaRenamer::run() 1";

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << "PtFastaRenamer.Run is executing";

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QStringList args   = parser.positionalArguments();

      QString odsFileStr = parser.value(correspOption);
      if(odsFileStr.isEmpty())
        {
          throw PappsoException(QObject::tr(
            "correspondande table is needed (ODS file) with option -c"));
        }

      bool include_all = false;
      if(parser.isSet(includeOption))
        {
          include_all = true;
        }

      QString fastaFileStr = parser.value(inputOption);
      QFile fastaFile;
      if(fastaFileStr.isEmpty())
        {
          fastaFile.open(stdin, QIODevice::ReadOnly);
        }
      else
        {
          fastaFile.setFileName(fastaFileStr);
          if(!fastaFile.open(QIODevice::ReadOnly))
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: unable to open %1 FASTA file for read")
                  .arg(fastaFileStr));
            }
        }

      QString fileStr = parser.value(outputOption);
      QTextStream *p_outputStream;
      QFile outFile;
      if(fileStr.isEmpty())
        {
          p_outputStream = new QTextStream(stdout, QIODevice::WriteOnly);
        }
      else
        {
          outFile.setFileName(fileStr);
          if(!outFile.open(QIODevice::WriteOnly))
            {
              throw pappso::PappsoException(
                QObject::tr("ERROR: unable to open %1 output file for write")
                  .arg(fileStr));
            }
          p_outputStream = new QTextStream(&outFile);
        }


      FastaOutputStream fasta_output(*p_outputStream);

      QFile odsFile(odsFileStr);
      if(!odsFile.open(QIODevice::ReadOnly))
        {
          throw pappso::PappsoException(
            QObject::tr("ERROR: unable to open %1 ODS file for read")
              .arg(odsFileStr));
        }
      MapOds map_ods;
      OdsDocReader ods_reader(map_ods);
      qDebug() << "ods_reader.parse(&odsFile)";
      ods_reader.parse(&odsFile);

      qDebug() << "renamer(fasta_output, map_ods.getMap(), include_all)";
      FastaRenamer renamer(fasta_output, map_ods.getMap(), include_all);
      FastaReader reader(renamer);
      reader.parse(&fastaFile);
      fastaFile.close();

      p_outputStream->flush();
      delete p_outputStream;
    }
  catch(OdsException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools, reading an "
                     "ODS file. Dont Panic :"
                  << endl;
      errorStream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.what() << endl;
      exit(1);
      app->exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
PtFastaRenamer::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
PtFastaRenamer::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << "main begin";
  QCoreApplication app(argc, argv);
  qDebug() << "main 1";
  QCoreApplication::setApplicationName("pt-fastarenamer");
  QCoreApplication::setApplicationVersion(PAPPSOMSTOOLS_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  PtFastaRenamer myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(
    &app, SIGNAL(aboutToQuit()), &myMain, SLOT(aboutToQuitApp()));
  qDebug() << "main 2";


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
