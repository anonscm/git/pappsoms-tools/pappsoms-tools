
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "pt_fastagrouper.h"

#include <iostream>
#include <QCoreApplication>
#include <QTimer>
#include <QDebug>
#include <QDateTime>
#include <QDir>
#include <QCommandLineParser>
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/fasta/fastareader.h>
#include <odsstream/odsdocwriter.h>
#include <odsstream/tsvdirectorywriter.h>
#include <odsstream/tsvoutputstream.h>

#include <vector>

using namespace pappso;


PtFastaGrouper::PtFastaGrouper(QObject *parent) : QObject(parent)
{
  // get the instance of the main application
  app = QCoreApplication::instance();
  // setup everything here
  // create any global objects
  // setup debug and warning mode
}


class PeptideReader : public FastaHandlerInterface
{
  public:
  PeptideReader(GrpExperiment &grpExperiment) : _grpExperiment(grpExperiment){};

  void
  setSequence(const QString &description, const QString &sequence) override
  {
    // qDebug() << "PeptideReader::setSequence " << description << " "
    // <<sequence;
    QStringList descr_split = description.simplified().split(" ");
    QString accession       = descr_split.at(0);
    GrpProteinSp proteinSp;
    if(descr_split.size() > 1)
      {
        descr_split.pop_front();
        proteinSp =
          _grpExperiment.getGrpProteinSp(accession, descr_split.join(" "));
      }
    else
      {
        proteinSp = _grpExperiment.getGrpProteinSp(accession, "");
      }
    QStringList list = sequence.split(" ");
    for(const QString &peptide : list)
      {
        // qDebug() << "PeptideReader::setSequence " << peptide;
        _grpExperiment.setGrpPeptide(proteinSp, peptide, 0);
      }
  };

  private:
  GrpExperiment &_grpExperiment;
};

void
PtFastaGrouper::writeGroupedProteinList(
  CalcWriterInterface &writer,
  std::vector<pappso::GrpProteinSpConst> protein_list)
{
  writer.writeSheet("grouped protein list");
  writer.writeCell("group number");
  writer.writeCell("accession");
  writer.writeCell("description");
  for(auto &&GrpProteinSp : protein_list)
    {
      writer.writeLine();
      writer.writeCell(GrpProteinSp.get()->getGroupingId());
      writer.writeCell(GrpProteinSp.get()->getAccession());
      writer.writeCell(GrpProteinSp.get()->getDescription());
    }
  writer.writeLine();
}

// 10ms after the application starts this method will run
// all QT messaging is running at this point so threads, signals and slots
// will all work as expected.
void
PtFastaGrouper::run()
{
  // ./src/pt-fastagrouper ../../pappsomspp/test/data/asr1_digested_peptides.txt
  QTextStream errorStream(stderr, QIODevice::WriteOnly);

  try
    {
      qDebug() << "PtFastaGrouper::run() begin";
      QCommandLineParser parser;

      // throw pappso::PappsoException("test");
      parser.setApplicationDescription(QString(SOFTWARE_NAME)
                                         .append(" ")
                                         .append(PAPPSOMSTOOLS_VERSION)
                                         .append(" FASTA grouper"));
      parser.addHelpOption();
      parser.addVersionOption();
      QCommandLineOption inputOption(
        QStringList() << "i"
                      << "fasta",
        QCoreApplication::translate("main", "FASTA file <input>."),
        QCoreApplication::translate("main", "input"));
      QCommandLineOption odsOption(
        QStringList() << "o"
                      << "ods",
        QCoreApplication::translate("main",
                                    "Write results in an ODS file <output>."),
        QCoreApplication::translate("main", "output"));
      QCommandLineOption tsvOption(
        QStringList() << "t"
                      << "tsv",
        QCoreApplication::translate(
          "main", "Write results in TSV files contained in a <directory>."),
        QCoreApplication::translate("main", "directory"), QString());
      parser.addOption(inputOption);
      parser.addOption(odsOption);
      parser.addOption(tsvOption);

      qDebug() << "PtFastaGrouper::run() 1";

      // Process the actual command line arguments given by the user
      parser.process(*app);

      // QCoreApplication * app(this);
      // Add your main code here
      qDebug() << "PtFastaGrouper.Run is executing";

      const QDateTime dt_begin = QDateTime::currentDateTime();
      const QFileInfo masschroqprm_dir_path(
        QCoreApplication::applicationDirPath());
      const QStringList args = parser.positionalArguments();

      QString fastaFileStr = parser.value(inputOption);
      QFile fastaFile;
      if(fastaFileStr.isEmpty())
        {
          fastaFile.open(stdin, QIODevice::ReadOnly);
        }
      else
        {
          fastaFile.setFileName(fastaFileStr);
          fastaFile.open(QIODevice::ReadOnly);
        }


      GrpGroupingMonitor monitor;
      GrpExperiment grpExperiment(&monitor);
      PeptideReader peptideReader(grpExperiment);
      FastaReader reader(peptideReader);
      reader.parse(&fastaFile);
      fastaFile.close();
      grpExperiment.startGrouping();

      std::vector<GrpProteinSpConst> protein_list =
        grpExperiment.getGrpProteinSpList();
      // readMzXml();
      qDebug() << "PtFastaGrouper::run() end";


      QString odsFileStr = parser.value(odsOption);
      QString tsvFileStr = parser.value(tsvOption);
      if(tsvFileStr.isEmpty() && odsFileStr.isEmpty())
        {
          QTextStream outputStream(stdout, QIODevice::WriteOnly);
          TsvOutputStream writer(outputStream);
          writeGroupedProteinList(writer, protein_list);
          writer.close();
        }
      if(!tsvFileStr.isEmpty())
        {
          QDir directory(tsvFileStr);
          qDebug() << "QDir directory(tsvFileStr) " << directory.absolutePath()
                   << " " << tsvFileStr;
          // file.open(QIODevice::WriteOnly);
          TsvDirectoryWriter writer(directory);
          writeGroupedProteinList(writer, protein_list);
          // quantificator.report(writer);
          writer.close();
        }
      if(!odsFileStr.isEmpty())
        {
          QFile file(odsFileStr);
          // file.open(QIODevice::WriteOnly);
          OdsDocWriter writer(&file);
          // quantificator.report(writer);
          writeGroupedProteinList(writer, protein_list);
          writer.close();
          file.close();
        }
    }
  catch(pappso::PappsoException &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.qwhat() << endl;
      exit(1);
      app->exit(1);
    }

  catch(std::exception &error)
    {
      errorStream << "Oops! an error occurred in PAPPSO MS tools. Dont Panic :"
                  << endl;
      errorStream << error.what() << endl;
      exit(1);
      app->exit(1);
    }


  // you must call quit when complete or the program will stay in the
  // messaging loop
  quit();
}

// call this routine to quit the application
void
PtFastaGrouper::quit()
{
  // you can do some cleanup here
  // then do emit finished to signal CoreApplication to quit
  emit finished();
}

// shortly after quit is called the CoreApplication will signal this routine
// this is a good place to delete any objects that were created in the
// constructor and/or to stop any threads
void
PtFastaGrouper::aboutToQuitApp()
{
  // stop threads
  // sleep(1);   // wait for threads to stop.
  // delete any objects
}


int
main(int argc, char **argv)
{
  // QTextStream consoleErr(stderr);
  // QTextStream consoleOut(stdout, QIODevice::WriteOnly);
  // ConsoleOut::setCout(new QTextStream(stdout, QIODevice::WriteOnly));
  // ConsoleOut::setCerr(new QTextStream(stderr, QIODevice::WriteOnly));
  qDebug() << "main begin";
  QCoreApplication app(argc, argv);
  qDebug() << "main 1";
  QCoreApplication::setApplicationName("pt-fastagrouper");
  QCoreApplication::setApplicationVersion(PAPPSOMSTOOLS_VERSION);
  QLocale::setDefault(QLocale::system());

  // create the main class
  PtFastaGrouper myMain;
  // connect up the signals
  QObject::connect(&myMain, SIGNAL(finished()), &app, SLOT(quit()));
  QObject::connect(&app, SIGNAL(aboutToQuit()), &myMain,
                   SLOT(aboutToQuitApp()));
  qDebug() << "main 2";


  // This code will start the messaging engine in QT and in
  // 10ms it will start the execution in the MainClass.run routine;
  QTimer::singleShot(10, &myMain, SLOT(run()));
  return app.exec();
}
