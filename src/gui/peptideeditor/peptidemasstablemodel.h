
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <QAbstractTableModel>
#include <pappsomspp/peptide/peptide.h>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/peptide/peptidenaturalisotopeaverage.h>

class PeptideMassTableModel : public QAbstractTableModel
{
  Q_OBJECT
  public:
  PeptideMassTableModel(QObject *parent = 0);

  void setPeptideSp(pappso::NoConstPeptideSp &peptide_sp);
  void setPeptideCharge(int charge);
  void setPrecision(pappso::PrecisionPtr p_precision);

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant headerData(int section, Qt::Orientation orientation,
                      int role) const override;
  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;

  signals:
  void isotopeMassListChanged(
    std::vector<pappso::PeptideNaturalIsotopeAverageSp> isotope_mass_list);

  private:
  void update();

  private:
  pappso::NoConstPeptideSp _peptide_sp;
  unsigned int _charge;
  pappso::PrecisionPtr _p_isotope_rank_precision;
  std::vector<pappso::PeptideNaturalIsotopeAverageSp> _isotope_mass_list;
};
