
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptidemasstablemodel.h"
//#include <iostream>
#include <QDebug>
#include <map>
#include <pappsomspp/peptide/peptidenaturalisotopelist.h>


PeptideMassTableModel::PeptideMassTableModel(QObject *parent)
  : QAbstractTableModel(parent)
{

  // ui->tableView->show();
  // QModelIndex topLeft = createIndex(0,0);
  // emit a signal to make the view reread identified data
  // emit dataChanged(topLeft, topLeft);
  _p_isotope_rank_precision = pappso::PrecisionFactory::getPpmInstance(2);
  _charge                   = 1;
}

void
PeptideMassTableModel::update()
{
  qDebug() << " " << _p_isotope_rank_precision->toString();
  _isotope_mass_list.resize(0);
  // compute isotope masses :
  if(_peptide_sp != nullptr)
    {
      pappso::PeptideNaturalIsotopeList isotopeList(_peptide_sp);
      std::map<unsigned int, pappso::pappso_double> map_isotope_number =
        isotopeList.getIntensityRatioPerIsotopeNumber();

      for(unsigned int i = 0; i < map_isotope_number.size(); i++)
        {

          unsigned int asked_rank = 0;
          unsigned int given_rank = 0;
          bool more_rank          = true;
          while(more_rank)
            {
              asked_rank++;
              pappso::PeptideNaturalIsotopeAverage isotopeAverageMono(
                isotopeList, asked_rank, i, _charge, _p_isotope_rank_precision);
              given_rank = isotopeAverageMono.getIsotopeRank();
              if(given_rank < asked_rank)
                {
                  more_rank = false;
                }
              else if(isotopeAverageMono.getIntensityRatio() == 0)
                {
                  more_rank = false;
                }
              else
                {
                  // isotopeAverageMono.makePeptideNaturalIsotopeAverageSp();
                  _isotope_mass_list.push_back(
                    isotopeAverageMono.makePeptideNaturalIsotopeAverageSp());
                }
            }
        }
      qDebug();
      emit isotopeMassListChanged(_isotope_mass_list);
      // we identify the top left cell
      QModelIndex topLeft = createIndex(0, 0);
      // emit a signal to make the view reread identified data
      emit headerDataChanged(Qt::Horizontal, 0, _peptide_sp.get()->size() - 1);
      emit dataChanged(topLeft, topLeft);
      qDebug();
    }
  qDebug();
}

void
PeptideMassTableModel::setPrecision(pappso::PrecisionPtr p_precision)
{
  qDebug();
  _p_isotope_rank_precision = p_precision;
  update();
  qDebug();
}

void
PeptideMassTableModel::setPeptideSp(pappso::NoConstPeptideSp &peptide_sp)
{
  qDebug();
  _peptide_sp = peptide_sp;
  update();
  qDebug();
}


void
PeptideMassTableModel::setPeptideCharge(int charge)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " charge=" << charge;
  _charge = charge;
  update();
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " charge=" << charge;
}

int
PeptideMassTableModel::rowCount(const QModelIndex &parent) const
{
  qDebug();
  if(_peptide_sp.get() == nullptr)
    {
      qDebug();
      return 0;
    }
  qDebug();
  return 2;
}
int
PeptideMassTableModel::columnCount(const QModelIndex &parent) const
{
  qDebug();
  if(_peptide_sp.get() == nullptr)
    {
      qDebug();
      return 0;
    }
  qDebug() << _isotope_mass_list.size();
  return 1 + _isotope_mass_list.size();
}
QVariant
PeptideMassTableModel::headerData(int section,
                                  Qt::Orientation orientation,
                                  int role) const
{
  // qDebug();
  if(_peptide_sp.get() != nullptr)
    {
      // qDebug() << "PeptideTableModel::headerData";
      if(role == Qt::DisplayRole)
        {
          if(orientation == Qt::Horizontal)
            {
              if(section == 0)
                {
                  // qDebug();
                  return QString("m/z");
                }
              else
                {
                  if(section < _isotope_mass_list.size())
                    {
                      // qDebug();
                      return QString("isotope %1-%2")
                        .arg(_isotope_mass_list.at(section)
                               .get()
                               ->getIsotopeNumber())
                        .arg(_isotope_mass_list.at(section)
                               .get()
                               ->getIsotopeRank());
                    }
                }
            }
        }
    }
  // qDebug();
  return QVariant();
}
QVariant
PeptideMassTableModel::data(const QModelIndex &index, int role) const
{
  qDebug();
  // generate a log message when this method gets called
  if(_peptide_sp.get() != nullptr)
    {
      int row = index.row();
      int col = index.column();
      // qDebug() << QString("row %1, col%2, role %3")
      //         .arg(row).arg(col).arg(role);
      switch(role)
        {
          case Qt::TextAlignmentRole:
            {
              if(index.row() == 0)
                {
                  return (Qt::AlignHCenter);
                }
              return QVariant();
              break;
            }
          case Qt::DisplayRole:
            {
              if(index.column() == 0)
                { // filename
                  // return
                  //    _current_directory.entryInfoList().at(index.row()).fileName();
                }
              if(row == 0)
                {
                  if(col == 0)
                    {
                      return QString("%1").arg(QString::number(
                        _peptide_sp.get()->getMz(_charge), 'g', 10));
                    }
                  if(col < _isotope_mass_list.size())
                    {
                      return QString("%1").arg(QString::number(
                        _isotope_mass_list.at(col).get()->getMz(), 'g', 10));
                    }
                }
              if(row == 1)
                {
                  if(col < _isotope_mass_list.size())
                    {
                      return QString("%1").arg(
                        _isotope_mass_list.at(col).get()->getIntensityRatio());
                    }
                }
            }
        }
    }
  qDebug();
  return QVariant();
}
