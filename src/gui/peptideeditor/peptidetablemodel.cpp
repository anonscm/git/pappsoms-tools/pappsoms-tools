
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptidetablemodel.h"


#include <QDebug>
#include <pappsomspp/pappsoexception.h>


PeptideTableModel::PeptideTableModel(QObject *parent)
  : QAbstractTableModel(parent)
{

  // ui->tableView->show();
  // QModelIndex topLeft = createIndex(0,0);
  // emit a signal to make the view reread identified data
  // emit dataChanged(topLeft, topLeft);
}


void
PeptideTableModel::setPeptideSp(pappso::NoConstPeptideSp &peptide_sp)
{
  qDebug();
  _peptide_sp = peptide_sp;
  // we identify the top left cell
  QModelIndex topLeft = createIndex(0, 0);
  // emit a signal to make the view reread identified data
  emit headerDataChanged(Qt::Horizontal, 0, _peptide_sp.get()->size() - 1);
  emit dataChanged(topLeft, topLeft);
  qDebug();
}

int
PeptideTableModel::rowCount(const QModelIndex &parent) const
{
  qDebug();
  if(_peptide_sp.get() == nullptr)
    {
      qDebug();
      return 0;
    }
  unsigned int max_mod = 0;
  for(auto &aa : *_peptide_sp.get())
    {
      if(max_mod < aa.getModificationList().size())
        {
          max_mod = aa.getModificationList().size();
        }
    }
  qDebug();
  return max_mod + 1;
}
int
PeptideTableModel::columnCount(const QModelIndex &parent) const
{
  qDebug();
  if(_peptide_sp.get() == nullptr)
    {
      qDebug();
      return 0;
    }
  qDebug();
  return _peptide_sp.get()->size();
}
QVariant
PeptideTableModel::headerData(int section,
                              Qt::Orientation orientation,
                              int role) const
{
  // qDebug() << "PeptideTableModel::headerData";
  qDebug();
  try
    {
      if(role == Qt::DisplayRole)
        {
          if(orientation == Qt::Horizontal)
            {
              qDebug();
              if((_peptide_sp.get() != nullptr) &&
                 (section < _peptide_sp.get()->size()))
                {
                  qDebug();
                  return QString("%1").arg(
                    _peptide_sp.get()->getAa(section).getLetter());
                }
            }
        }
    }
  catch(pappso::PappsoException &exception)
    {
      qDebug() << exception.qwhat();
    }
  return QVariant();
}

QVariant
PeptideTableModel::data(const QModelIndex &index, int role) const
{
  // generate a log message when this method gets called
  int row = index.row();
  int col = index.column();
  // qDebug() << QString("row %1, col%2, role %3")
  //         .arg(row).arg(col).arg(role);
  qDebug();
  try
    {
      switch(role)
        {
          case Qt::TextAlignmentRole:
            {
              if(index.row() == 0)
                {
                  return (Qt::AlignHCenter);
                }
              return QVariant();
              break;
            }
          case Qt::DisplayRole:
            {
              if(index.column() == 0)
                { // filename
                  // return
                  //    _current_directory.entryInfoList().at(index.row()).fileName();
                }
              if(index.row() == 0)
                {
                  return QString("%1").arg(index.column() + 1);
                }
              else
                {

                  qDebug();
                  auto pt_mod = _peptide_sp.get()
                                  ->getAa(index.column())
                                  .getModificationList()
                                  .begin();
                  auto pt_mod_end = _peptide_sp.get()
                                      ->getAa(index.column())
                                      .getModificationList()
                                      .end();


                  qDebug();
                  unsigned int i = 1;
                  while((pt_mod != pt_mod_end) && (i < index.row()))
                    {
                      pt_mod++;
                      i++;
                    }

                  qDebug();
                  if(pt_mod == pt_mod_end)
                    {
                      qDebug();
                      return QVariant();
                    }
                  //+ (index.row()-1);
                  if((*pt_mod)->isInternal())
                    {

                      qDebug();
                      return QString("%1").arg((*pt_mod)->getName());
                    }

                  qDebug();
                  return QString("[%1] %2 \n%3")
                    .arg((*pt_mod)->getAccession())
                    .arg((*pt_mod)->getName())
                    .arg(QString::number((*pt_mod)->getMass(), 'g', 15));
                }
            }
        }
    }
  catch(pappso::PappsoException &exception)
    {
      qDebug() << exception.qwhat();
    }
  qDebug();
  return QVariant();
}
