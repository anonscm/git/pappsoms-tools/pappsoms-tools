
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <QMainWindow>
#include <QThread>
#include <QFileInfo>
#include <pappsomspp/types.h>
#include <pappsomspp/peptide/peptide.h>
#include "iontableviewer/iontablewindow.h"
#include "spectrumviewer/spectrumwindow.h"
#include "spectrumparam/spectrumparam.h"
#include "peptideeditor/peptidetablemodel.h"
#include "peptideeditor/peptidemasstablemodel.h"
#include "../utils/fragmentator.h"


namespace Ui
{
class PeptideViewer;
}


class PeptideViewer : public QMainWindow
{
  Q_OBJECT

  public:
  explicit PeptideViewer(QWidget *parent = 0);
  ~PeptideViewer();

  public slots:
  void peptideEdited(QString peptideStr);
  void setPrecision(PrecisionPtr p_precision);

  public slots:
  void peptideChargeEdited(int charge);
  signals:
  void peptideChanged(PeptideSp peptide);
  signals:
  void peptideChargeChanged(unsigned int charge);

  private:
  Ui::PeptideViewer *ui;
  SpectrumWindow *_p_spectrum_window;
  SpectrumParam *_p_spectrum_param;
  IonTableWindow *_p_ion_tablewindow;
  PeptideTableModel *_p_peptide_table_model;
  Fragmentator *_p_fragmentator;
  PeptideMassTableModel *_p_peptide_mass_table_model;
};
