
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#include <QFileDialog>
#include <QSettings>
#include <QDockWidget>
#include <QCoreApplication>
#include <QStringList>
#include "peptideviewer.h"
#include <pappsomspp/peptide/peptidestrparser.h>
#include "ui_peptide_viewer.h"


PeptideViewer::PeptideViewer(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::PeptideViewer)
{
  ui->setupUi(this);

  _p_fragmentator = new Fragmentator();


  QDockWidget *dock  = new QDockWidget(tr("Spectrum Viewer"), this);
  _p_spectrum_window = new SpectrumWindow(this);
  //_protein_list_window->show();
  dock->setWidget(_p_spectrum_window);
  addDockWidget(Qt::RightDockWidgetArea, dock);

  dock              = new QDockWidget(tr("Spectrum Param"), this);
  _p_spectrum_param = new SpectrumParam(this);
  //_protein_list_window->show();
  dock->setWidget(_p_spectrum_param);
  addDockWidget(Qt::LeftDockWidgetArea, dock);


  dock               = new QDockWidget(tr("Ion table"), this);
  _p_ion_tablewindow = new IonTableWindow(this);
  //_protein_list_window->show();
  dock->setWidget(_p_ion_tablewindow);
  addDockWidget(Qt::RightDockWidgetArea, dock);


  _p_peptide_table_model = new PeptideTableModel();

  ui->peptideTableView->setModel(_p_peptide_table_model);

  _p_peptide_mass_table_model = new PeptideMassTableModel();

  ui->peptideMassTable->setModel(_p_peptide_mass_table_model);
#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(this,
          &PeptideViewer::peptideChanged,
          _p_spectrum_window,
          &SpectrumWindow::setPeptideSp);
  connect(this,
          &PeptideViewer::peptideChanged,
          _p_fragmentator,
          &Fragmentator::setPeptideSp);
  connect(this,
          &PeptideViewer::peptideChanged,
          _p_spectrum_param,
          &SpectrumParam::setPeptideSp);
  connect(this,
          &PeptideViewer::peptideChargeChanged,
          _p_spectrum_param,
          &SpectrumParam::setPeptideCharge);
  connect(this,
          &PeptideViewer::peptideChargeChanged,
          _p_spectrum_window,
          &SpectrumWindow::setPeptideCharge);
  connect(this,
          &PeptideViewer::peptideChargeChanged,
          _p_ion_tablewindow,
          &IonTableWindow::setPeptideCharge);
  connect(_p_fragmentator,
          &Fragmentator::peptideFragmentIonListChanged,
          _p_ion_tablewindow,
          &IonTableWindow::setPeptideFragmentIonList);
  connect(_p_spectrum_window,
          &SpectrumWindow::mzChanged,
          _p_ion_tablewindow,
          &IonTableWindow::setMz);
  connect(_p_spectrum_param,
          &SpectrumParam::msmsPrecisionChanged,
          _p_spectrum_window,
          &SpectrumWindow::setPrecision);
  connect(_p_spectrum_param,
          &SpectrumParam::filteredSpectrumSpChanged,
          _p_spectrum_window,
          &SpectrumWindow::setFilteredSpectrumSp);
  connect(_p_spectrum_param,
          &SpectrumParam::msmsPrecisionChanged,
          _p_ion_tablewindow,
          &IonTableWindow::setPrecision);
  connect(_p_spectrum_param,
          &SpectrumParam::msmsPrecisionChanged,
          this,
          &PeptideViewer::setPrecision);
  connect(_p_spectrum_window,
          &SpectrumWindow::spectrumChanged,
          _p_spectrum_param,
          &SpectrumParam::setSpectrum);
  connect(_p_spectrum_window,
          &SpectrumWindow::spectrumChanged,
          _p_ion_tablewindow,
          &IonTableWindow::setSpectrum);
  connect(_p_peptide_mass_table_model,
          &PeptideMassTableModel::isotopeMassListChanged,
          _p_spectrum_window,
          &SpectrumWindow::setIsotopeMassList);

#else
  // Qt4 code
  connect(this,
          SIGNAL(peptideChanged(PeptideSp)),
          _p_spectrum_window,
          SLOT(setPeptideSp(PeptideSp)));
  connect(this,
          SIGNAL(peptideChanged(PeptideSp)),
          _p_fragmentator,
          SLOT(setPeptideSp(PeptideSp)));
  connect(this,
          SIGNAL(peptideChanged(PeptideSp)),
          _p_spectrum_param,
          SLOT(setPeptideSp(PeptideSp)));
  connect(this,
          SIGNAL(peptideChargeChanged(unsigned int)),
          _p_spectrum_param,
          SLOT(setPeptideCharge(unsigned int)));
  connect(this,
          SIGNAL(peptideChargeChanged(unsigned int)),
          _p_ion_tablewindow,
          SLOT(setPeptideCharge(unsigned int)));
  connect(_p_fragmentator,
          SIGNAL(peptideFragmentIonListChanged(PeptideFragmentIonListBaseSp)),
          _p_ion_tablewindow,
          SLOT(setPeptideFragmentIonList(PeptideFragmentIonListBaseSp)));
  connect(_p_spectrum_window,
          SIGNAL(mzChanged(double)),
          _p_ion_tablewindow,
          SLOT(setMz(double)));
  connect(_p_spectrum_param,
          SIGNAL(msmsPrecisionChanged(PrecisionP)),
          _p_spectrum_window,
          SLOT(setPrecision(PrecisionP)));
  connect(_p_spectrum_param,
          SIGNAL(filteredSpectrumSpChanged(SpectrumSp)),
          _p_spectrum_window,
          SLOT(setFilteredSpectrumSp(SpectrumSp)));
  connect(_p_spectrum_param,
          SIGNAL(msmsPrecisionChanged(PrecisionP)),
          _p_ion_tablewindow,
          SLOT(setPrecision(PrecisionP)));
  connect(_p_spectrum_param,
          SIGNAL(msmsPrecisionChanged(PrecisionP)),
          this,
          SLOT(setPrecision(PrecisionP)));
  connect(_p_spectrum_window,
          SIGNAL(spectrumChanged(pappso::QualifiedSpectrum)),
          _p_spectrum_param,
          SLOT(setSpectrum(pappso::QualifiedSpectrum)));
  connect(_p_spectrum_window,
          SIGNAL(spectrumChanged(pappso::QualifiedSpectrum)),
          _p_ion_tablewindow,
          SLOT(setSpectrum(pappso::QualifiedSpectrum)));
  connect(_p_peptide_mass_table_model,
          SIGNAL(isotopeMassListChanged(
            std::vector<pappso::PeptideNaturalIsotopeAverageSp>)),
          _p_spectrum_window,
          SLOT(setIsotopeMassList(
            std::vector<pappso::PeptideNaturalIsotopeAverageSp>)));

#endif
  /*
   */

  QCoreApplication *application = QCoreApplication::instance();

  QStringList arguments = application->arguments();

  QSettings settings;

  qDebug() << "PeptideViewer::PeptideViewer " << arguments.size() << " "
           << arguments.join(" ");
  if(arguments.size() == 5)
    {
      settings.setValue("peptide/string", arguments[1]);
      settings.setValue("peptide/charge", arguments[2]);
      qDebug() << "PeptideViewer::PeptideViewer "
               << settings.value("peptide/string", "");
      qDebug() << "PeptideViewer::PeptideViewer "
               << settings.value("peptide/charge", "");
    }
  ui->peptideEdit->setText(settings.value("peptide/string", "").toString());
  ui->chargeSpinBox->setValue(settings.value("peptide/charge", "1").toInt());


  _p_spectrum_param->initSignals();
  if(arguments.size() == 5)
    {
      _p_spectrum_window->setMzDataFile(arguments[3]);
      _p_spectrum_window->setScanNum(arguments[4].toInt());
    }
  // peptideEdited(settings.value("peptide/string", "").toString());
}

PeptideViewer::~PeptideViewer()
{
  if(_p_spectrum_window != nullptr)
    delete _p_spectrum_window;
  delete ui;
  if(_p_peptide_table_model != nullptr)
    delete _p_peptide_table_model;
  if(_p_peptide_mass_table_model != nullptr)
    delete _p_peptide_mass_table_model;
}
void
PeptideViewer::peptideEdited(QString peptideStr)
{
  qDebug() << peptideStr;
  try
    {
      if(!peptideStr.isEmpty())
        {
          QSettings settings;
          settings.setValue("peptide/string", peptideStr);

          qDebug() << peptideStr;
          NoConstPeptideSp peptideSp =
            PeptideStrParser::parseNoConstString(peptideStr);

          qDebug() << peptideStr;
          _p_peptide_table_model->setPeptideSp(peptideSp);

          qDebug() << peptideStr;
          _p_peptide_mass_table_model->setPeptideSp(peptideSp);

          qDebug() << peptideStr;
          ui->mass_label->setText(
            QString::number(peptideSp.get()->getMass(), 'g', 15));
          ui->formula_label->setText(peptideSp.get()->getFormula(0));

          qDebug() << peptideStr;
          emit peptideChanged(peptideSp);
        }
    }
  catch(PappsoException exception)
    {
      qDebug() << " PeptideViewer::peptideEdited " << exception.qwhat();
    }
  catch(std::exception exception)
    {
      qDebug() << " PeptideViewer::peptideEdited " << exception.what();
    }
  qDebug() << peptideStr;
}
void
PeptideViewer::peptideChargeEdited(int charge)
{
  qDebug() << " charge=" << charge;
  QSettings settings;
  settings.setValue("peptide/charge", QString("%1").arg(charge));
  _p_peptide_mass_table_model->setPeptideCharge(charge);
  qDebug() << " charge=" << charge;
  peptideChargeChanged(charge);
  qDebug() << " charge=" << charge;
}


void
PeptideViewer::setPrecision(PrecisionPtr p_precision)
{
  qDebug() << p_precision->toString();
  // std::cout << "PeptideViewer::setPrecision";
  _p_peptide_mass_table_model->setPrecision(p_precision);
  qDebug();
}
