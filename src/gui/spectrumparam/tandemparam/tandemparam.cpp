
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "tandemparam.h"
#include "ui_tandemparam.h"
#include <QSettings>
#include <QDebug>

TandemParam::TandemParam(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::TandemParam)
{
  ui->setupUi(this);

  qDebug();

  qDebug() << _spectrum_process.getDynamicRange();

  QSettings settings;
  // settings.setValue("xtandem/spectrum_neutral_loss_mass",
  // QString::number(_spectrum_process.getNeutralLossMass(), 'g', 10) );


  _spectrum_process.setRemoveIsotope(
    settings
      .value("xtandem/spectrum_remove_isotope",
             _spectrum_process.getRemoveIsotope())
      .toBool());
  _spectrum_process.setDynamicRange(
    settings
      .value("xtandem/spectrum_dynamic_range",
             _spectrum_process.getDynamicRange())
      .toDouble());
  _spectrum_process.setMinimumMz(
    settings
      .value("xtandem/spectrum_minimum_mz", _spectrum_process.getMinimumMz())
      .toDouble());
  _spectrum_process.setNmostIntense(
    settings
      .value("xtandem/spectrum_nmost_intense",
             _spectrum_process.getNmostIntense())
      .toInt());
  _spectrum_process.setExcludeParent(
    settings
      .value("xtandem/spectrum_exclude_parent",
             _spectrum_process.getExcludeParent())
      .toBool());
  _spectrum_process.setExcludeParentNeutralLoss(
    settings
      .value("xtandem/spectrum_exclude_neutral_loss",
             _spectrum_process.getExcludeParentNeutralLoss())
      .toBool());
  _spectrum_process.setNeutralLossMass(
    settings
      .value("xtandem/spectrum_neutral_loss_mass",
             QString::number(_spectrum_process.getNeutralLossMass(), 'g', 10))
      .toDouble());
  _spectrum_process.setNeutralLossWindowDalton(
    settings
      .value("xtandem/spectrum_neutral_loss_window_dalton",
             _spectrum_process.getNeutralLossWindowDalton())
      .toDouble());
  _spectrum_process.setRefineSpectrumModel(
    settings
      .value("xtandem/spectrum_refine_model",
             _spectrum_process.getRefineSpectrumModel())
      .toBool());
  _spectrum_process.setIonScore(
    PeptideIon::y,
    settings
      .value("xtandem/spectrum_y_ions",
             _spectrum_process.getIonScore(PeptideIon::y))
      .toBool());
  _spectrum_process.setIonScore(
    PeptideIon::b,
    settings
      .value("xtandem/spectrum_b_ions",
             _spectrum_process.getIonScore(PeptideIon::b))
      .toBool());
  _spectrum_process.setIonScore(
    PeptideIon::yo,
    settings
      .value("xtandem/spectrum_yo_ions",
             _spectrum_process.getIonScore(PeptideIon::yo))
      .toBool());
  _spectrum_process.setIonScore(
    PeptideIon::bo,
    settings
      .value("xtandem/spectrum_bo_ions",
             _spectrum_process.getIonScore(PeptideIon::bo))
      .toBool());
  _spectrum_process.setIonScore(
    PeptideIon::ystar,
    settings
      .value("xtandem/spectrum_ystar_ions",
             _spectrum_process.getIonScore(PeptideIon::ystar))
      .toBool());
  _spectrum_process.setIonScore(
    PeptideIon::bstar,
    settings
      .value("xtandem/spectrum_bstar_ions",
             _spectrum_process.getIonScore(PeptideIon::bstar))
      .toBool());
  _spectrum_process.setIonScore(
    PeptideIon::c,
    settings
      .value("xtandem/spectrum_c_ions",
             _spectrum_process.getIonScore(PeptideIon::c))
      .toBool());
  _spectrum_process.setIonScore(
    PeptideIon::z,
    settings
      .value("xtandem/spectrum_z_ions",
             _spectrum_process.getIonScore(PeptideIon::z))
      .toBool());
  _spectrum_process.setIonScore(
    PeptideIon::a,
    settings
      .value("xtandem/spectrum_a_ions",
             _spectrum_process.getIonScore(PeptideIon::a))
      .toBool());
  _spectrum_process.setIonScore(
    PeptideIon::x,
    settings
      .value("xtandem/spectrum_x_ions",
             _spectrum_process.getIonScore(PeptideIon::x))
      .toBool());
  //_spectrum_process.setIonScore(PeptideIon::b,settings.value("xtandem/spectrum_b_ions",
  //_spectrum_process.getIonScore(PeptideIon::b)).toBool()); bool _x_ions =
  // false; //CO2

  qDebug() << "TandemParam::TandemParam _spectrum_process.getDynamicRange() "
           << _spectrum_process.getDynamicRange();

  ui->dynamicRangeSpinBox->setValue(_spectrum_process.getDynamicRange());
  ui->minimumMzSpinBox->setValue(_spectrum_process.getMinimumMz());
  ui->removeIsotopeBox->setChecked(_spectrum_process.getRemoveIsotope());
  ui->nmostIntenseSpinBox->setValue(_spectrum_process.getNmostIntense());
  ui->excludeParentCheckBox->setChecked(_spectrum_process.getExcludeParent());
  ui->excludeNeutralLossCheckBox->setChecked(
    _spectrum_process.getExcludeParentNeutralLoss());
  ui->neutralLossMassSpinBox->setValue(_spectrum_process.getNeutralLossMass());
  ui->neutralLossWindowDaltonSpinBox->setValue(
    _spectrum_process.getNeutralLossWindowDalton());
  ui->refineSpectrumModelCheckBox->setChecked(
    _spectrum_process.getRefineSpectrumModel());
  ui->aIonCheckBox->setChecked(_spectrum_process.getIonScore(PeptideIon::a));
  ui->bIonCheckBox->setChecked(_spectrum_process.getIonScore(PeptideIon::b));
  ui->boIonCheckBox->setChecked(_spectrum_process.getIonScore(PeptideIon::bo));
  ui->cIonCheckBox->setChecked(_spectrum_process.getIonScore(PeptideIon::c));
  ui->yIonCheckBox->setChecked(_spectrum_process.getIonScore(PeptideIon::y));
  ui->yoIonCheckBox->setChecked(_spectrum_process.getIonScore(PeptideIon::yo));
  ui->zIonCheckBox->setChecked(_spectrum_process.getIonScore(PeptideIon::z));
  ui->bstarCheckBox->setChecked(
    _spectrum_process.getIonScore(PeptideIon::bstar));
  ui->ystarCheckBox->setChecked(
    _spectrum_process.getIonScore(PeptideIon::ystar));
  ui->xIonCheckBox->setChecked(_spectrum_process.getIonScore(PeptideIon::x));

  excludeNeutralLossChecked(_spectrum_process.getExcludeParentNeutralLoss());

  qDebug();
}

TandemParam::~TandemParam()
{
}

void
TandemParam::closeEvent(QCloseEvent *event)
{
  event->ignore();
  this->hide();
  /*
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, APP_NAME,
                                                                tr("Are you
    sure?\n"), QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
    }*/
}

XtandemSpectrumProcess
TandemParam::getXtandemSpectrumProcess() const
{
  return _spectrum_process;
}

void
TandemParam::removeIsotopeBoxClicked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setRemoveIsotope(isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_remove_isotope",
                    QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);

  qDebug();
}


void
TandemParam::minimumMzSpinBoxValueChanged(double min_mz)
{

  qDebug() << min_mz;
  _spectrum_process.setMinimumMz(min_mz);

  QSettings settings;
  settings.setValue("xtandem/spectrum_minimum_mz", QString("%1").arg(min_mz));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}
void
TandemParam::dynamicRangeSpinBoxValueChanged(double dynamic_range)
{

  qDebug() << dynamic_range;
  _spectrum_process.setDynamicRange(dynamic_range);

  QSettings settings;
  settings.setValue("xtandem/spectrum_dynamic_range",
                    QString("%1").arg(dynamic_range));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}
void
TandemParam::nmostIntenseSpinBoxValueChanged(int nmost_intense)
{

  qDebug() << nmost_intense;
  _spectrum_process.setDynamicRange(nmost_intense);

  QSettings settings;
  settings.setValue("xtandem/spectrum_nmost_intense",
                    QString("%1").arg(nmost_intense));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}


void
TandemParam::excludeParentIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setExcludeParent(isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_exclude_parent",
                    QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}

void
TandemParam::excludeNeutralLossChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setExcludeParentNeutralLoss(isChecked);

  if(isChecked)
    {
      ui->neutralLossWindowDaltonSpinBox->setEnabled(true);
      ui->neutralLossMassSpinBox->setEnabled(true);
      ui->neutralLossMassLabel->setEnabled(true);
      ui->neutralLossWindowLabel->setEnabled(true);
    }
  else
    {
      ui->neutralLossWindowDaltonSpinBox->setEnabled(false);
      ui->neutralLossMassSpinBox->setEnabled(false);
      ui->neutralLossMassLabel->setEnabled(false);
      ui->neutralLossWindowLabel->setEnabled(false);
    }

  QSettings settings;
  settings.setValue("xtandem/spectrum_exclude_neutral_loss",
                    QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}
void
TandemParam::neutralLossMassValueChanged(double value)
{

  qDebug() << value;
  _spectrum_process.setNeutralLossMass(value);

  QSettings settings;
  settings.setValue("xtandem/spectrum_neutral_loss_mass",
                    QString::number(value, 'g', 10));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}
void
TandemParam::neutralLossWindowDaltonValueChanged(double value)
{

  qDebug() << value;
  _spectrum_process.setNeutralLossWindowDalton(value);

  QSettings settings;
  settings.setValue("xtandem/spectrum_neutral_loss_window_dalton",
                    QString("%1").arg(value));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}


void
TandemParam::refineSpectrumModelChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setRefineSpectrumModel(isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_refine_model",
                    QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}


void
TandemParam::yIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setIonScore(PeptideIon::y, isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_y_ions", QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}

void
TandemParam::bIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setIonScore(PeptideIon::b, isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_b_ions", QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}


void
TandemParam::boIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setIonScore(PeptideIon::bo, isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_bo_ions", QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}

void
TandemParam::ystarIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setIonScore(PeptideIon::ystar, isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_ystar_ions",
                    QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}

void
TandemParam::yoIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setIonScore(PeptideIon::yo, isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_yo_ions", QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}

void
TandemParam::bstarIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setIonScore(PeptideIon::bstar, isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_bstar_ions",
                    QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}

void
TandemParam::cIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setIonScore(PeptideIon::c, isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_c_ions", QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}

void
TandemParam::zIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setIonScore(PeptideIon::z, isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_z_ions", QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}

void
TandemParam::aIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setIonScore(PeptideIon::a, isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_a_ions", QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}

void
TandemParam::xIonChecked(bool isChecked)
{

  qDebug() << isChecked;
  _spectrum_process.setIonScore(PeptideIon::x, isChecked);

  QSettings settings;
  settings.setValue("xtandem/spectrum_x_ions", QString("%1").arg(isChecked));
  emit xtandemSpectrumProcessChanged(_spectrum_process);
  qDebug();
}
