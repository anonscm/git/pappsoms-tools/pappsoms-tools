
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <QMainWindow>
#include <pappsomspp/mzrange.h>
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/psm/xtandem/xtandemspectrumprocess.h>

#include "tandemparam/tandemparam.h"

using namespace pappso;

namespace Ui
{
class SpectrumParam;
}

class SpectrumParam : public QMainWindow
{
  Q_OBJECT

  public:
  explicit SpectrumParam(QWidget *parent = 0);
  ~SpectrumParam();
  void initSignals();

  public slots:
  void setPrecision(pappso::PrecisionPtr precision);
  void tandemParamLaunchButtonPushed();
  void setSpectrum(pappso::QualifiedMassSpectrum spectrum);
  void setPeptideSp(PeptideSp peptide_sp);
  void setPeptideCharge(unsigned int charge);
  void setXtandemSpectrumProcess(XtandemSpectrumProcess xt_spectrum_process);
  signals:
  void msmsPrecisionChanged(PrecisionPtr p_precision);
  void filteredSpectrumSpChanged(MassSpectrumSPtr filtered_spectrum);

  private:
  void updatePsm();
  void computeHyperscore(unsigned int charge);

  private:
  Ui::SpectrumParam *ui;

  PeptideSp _peptide_sp;
  unsigned int _peptide_charge;
  pappso::QualifiedMassSpectrum _spectrum;
  XtandemSpectrumProcess _spectrum_process;


  PrecisionPtr _precision_p;

  TandemParam *_tandem_param_p = nullptr;
};
