
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "spectrumparam.h"
#include "ui_spectrumparam.h"
#include <ui_spectrumparam.h>
#include <QDebug>
#include <QSettings>
#include <cmath>
#include <pappsomspp/peptide/peptidefragmention.h>
#include <pappsomspp/psm/xtandem/xtandemhyperscore.h>
#include <pappsomspp/psm/morpheus/morpheusscore.h>
#include <pappsomspp/psm/experimental/ionisotoperatioscore.h>
//#include <iostream>


SpectrumParam::SpectrumParam(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::SpectrumParam)
{
  ui->setupUi(this);
  ui->spectrumIdLabel->setText("");
  ui->r2_label->setText("");
  QSettings settings;
  _precision_p = PrecisionFactory::fromString(
    settings.value("msms/precision", "0.02 dalton").toString());
  ui->precision_widget->setPrecision(_precision_p);

  connect(ui->precision_widget,
          &PrecisionWidget::precisionChanged,
          this,
          &SpectrumParam::setPrecision);

  if(_tandem_param_p == nullptr)
    {
      _tandem_param_p   = new TandemParam(this);
      _spectrum_process = _tandem_param_p->getXtandemSpectrumProcess();
      _tandem_param_p->hide();


#if QT_VERSION >= 0x050000
      // Qt5 code
      connect(_tandem_param_p,
              &TandemParam::xtandemSpectrumProcessChanged,
              this,
              &SpectrumParam::setXtandemSpectrumProcess);

#else
      // Qt4 code
      connect(_tandem_param_p,
              SIGNAL(xtandemSpectrumProcessChanged(XtandemSpectrumProcess)),
              this,
              SLOT(setXtandemSpectrumProcess(XtandemSpectrumProcess)));
#endif
    }


  emit msmsPrecisionChanged(_precision_p);
  updatePsm();
}

SpectrumParam::~SpectrumParam()
{
  delete ui;
}


void
SpectrumParam::initSignals()
{
  qDebug();
  emit msmsPrecisionChanged(_precision_p);
  updatePsm();
  qDebug();
}

void
SpectrumParam::setSpectrum(pappso::QualifiedMassSpectrum spectrum)
{
  qDebug() << " charge=" << spectrum.getPrecursorCharge();
  _spectrum = spectrum;
  // ui.label_3.->msLevelLabgel.getMsLevel();
  ui->spectrumIdLabel->setText(spectrum.getMassSpectrumId().getNativeId());
  ui->msLevelLabel->setText(QString("%1").arg(spectrum.getMsLevel()));

  ui->precursor_scan_num_label->setText(spectrum.getPrecursorNativeId());
  ui->precursorMzLabel->setText("");
  ui->precursorChargeLabel->setText("");
  if(spectrum.getMsLevel() > 1)
    {
      ui->precursorMzLabel->setText(
        QString::number(spectrum.getPrecursorMz(), 'g', 15));
      ui->precursorChargeLabel->setText(
        QString("%1").arg(spectrum.getPrecursorCharge()));
    }
  pappso_double seconds = spectrum.getRtInSeconds();
  pappso_double minutes = trunc(seconds / 60);
  pappso_double left    = seconds - (minutes * 60);
  ui->retentionTimeLabel->setText(
    QString("%1s (%2m %3s)").arg(seconds).arg(minutes).arg(left));

  // spectrum.getOriginalSpectrumSp().get()->getMaxIntensity();
  // spectrum.getOriginalSpectrumSp().get().begin();
  updatePsm();
  qDebug();
}

void
SpectrumParam::setPeptideSp(PeptideSp peptide_sp)
{
  qDebug();
  _peptide_sp = peptide_sp;
  updatePsm();
  qDebug();
}

void
SpectrumParam::setPeptideCharge(unsigned int charge)
{
  qDebug() << " charge=" << charge;
  _peptide_charge = charge;
  updatePsm();
  qDebug() << " charge=" << charge;
}

void
SpectrumParam::computeHyperscore(unsigned int charge)
{
  qDebug() << " charge=" << charge;
  std::list<PeptideIon> ion_list;
  if(_spectrum_process.getIonScore(PeptideIon::y))
    {
      ion_list.push_back(PeptideIon::y);
    }
  if(_spectrum_process.getIonScore(PeptideIon::yo))
    {
      ion_list.push_back(PeptideIon::yo);
    }
  if(_spectrum_process.getIonScore(PeptideIon::b))
    {
      ion_list.push_back(PeptideIon::b);
    }
  if(_spectrum_process.getIonScore(PeptideIon::bo))
    {
      ion_list.push_back(PeptideIon::bo);
    }
  if(_spectrum_process.getIonScore(PeptideIon::ystar))
    {
      ion_list.push_back(PeptideIon::ystar);
    }
  if(_spectrum_process.getIonScore(PeptideIon::bstar))
    {
      ion_list.push_back(PeptideIon::bstar);
    }
  if(_spectrum_process.getIonScore(PeptideIon::c))
    {
      ion_list.push_back(PeptideIon::c);
    }
  if(_spectrum_process.getIonScore(PeptideIon::z))
    {
      ion_list.push_back(PeptideIon::z);
    }
  if(_spectrum_process.getIonScore(PeptideIon::a))
    {
      ion_list.push_back(PeptideIon::a);
    }
  if(_spectrum_process.getIonScore(PeptideIon::x))
    {
      ion_list.push_back(PeptideIon::x);
    }
  qDebug() << " charge=" << charge << " " << _spectrum_process.getMinimumMz();
  MassSpectrum spectrum_simple = _spectrum_process.process(
    *_spectrum.getMassSpectrumSPtr(), _spectrum.getPrecursorMz(), charge);
  qDebug() << " charge=" << charge;
  XtandemHyperscore hyperscore_withxtspectrum(
    spectrum_simple,
    _peptide_sp,
    charge,
    _precision_p,
    ion_list,
    _spectrum_process.getRefineSpectrumModel());
  qDebug() << " XtandemHyperscore="
           << hyperscore_withxtspectrum.getHyperscore();
  float test_tandem =
    round(100 * hyperscore_withxtspectrum.getHyperscore()) / 100;

  qDebug();
  ui->xtandemHyperscore->setText(QString("%1").arg(test_tandem));

  qDebug();
  std::vector<PeptideIon> ion_list_morpheus(ion_list.begin(), ion_list.end());

  qDebug();
  MorpheusScore morpheus(*_spectrum.getMassSpectrumSPtr(),
                         _peptide_sp,
                         charge,
                         _precision_p,
                         ion_list_morpheus,
                         pappso::RawFragmentationMode::full);
  qDebug() << morpheus.getMorpheusScore();
  test_tandem = round(10000 * morpheus.getMorpheusScore()) / 10000;
  ui->morpheus_label->setText(QString("%1").arg(test_tandem));


  pappso::IonIsotopeRatioScore ratio_score(*_spectrum.getMassSpectrumSPtr(),
                                           _peptide_sp,
                                           charge,
                                           _precision_p,
                                           ion_list_morpheus);
  ui->r2_label->setText(
    QString("%1").arg(ratio_score.getIonIsotopeRatioScore()));

  MassSpectrumSPtr filtered_raw_spectrum =
    spectrum_simple.makeMassSpectrumSPtr();
  emit filteredSpectrumSpChanged(filtered_raw_spectrum);
  qDebug();
}


void
SpectrumParam::updatePsm()
{
  qDebug();
  ui->deltaMz->setText("");
  ui->deltaMhplus->setText("");
  ui->deltaPpm->setText("");
  ui->xtandemHyperscore->setText("");
  ui->r2_label->setText("");

  qDebug() << (_peptide_sp.get() != nullptr) << " && "
           << (_spectrum.getMassSpectrumSPtr() != nullptr);
  if(_peptide_sp != nullptr)
    {
      qDebug() << _peptide_sp.get()->toAbsoluteString();
      if(_spectrum.getMassSpectrumSPtr() != nullptr)
        {
          qDebug();
          if(_spectrum.getMsLevel() > 1)
            {
              unsigned int charge = _spectrum.getPrecursorCharge();
              if(charge == 0)
                {
                  charge = _peptide_charge;
                }
              if(charge > 0)
                {
                  pappso_double prec_mz = _spectrum.getPrecursorMz();
                  // observed - theoretical
                  pappso_double delta_mz =
                    prec_mz - _peptide_sp.get()->getMz(charge);
                  ui->deltaMz->setText(QString::number(delta_mz, 'g', 15));
                  pappso_double observed_mh =
                    ((prec_mz - ((MHPLUS * (pappso_double)charge) /
                                 (pappso_double)charge)) *
                     (pappso_double)charge) +
                    MHPLUS;
                  ui->deltaMhplus->setText(QString::number(
                    observed_mh - _peptide_sp.get()->getMz(1), 'g', 15));

                  pappso_double delta_ppm =
                    (delta_mz / _peptide_sp.get()->getMz(charge)) * ONEMILLION;
                  ui->deltaPpm->setText(QString::number(delta_ppm, 'g', 15));

                  computeHyperscore(charge);
                }
            }
        }
    }
  qDebug();
}
void
SpectrumParam::setPrecision(PrecisionPtr precision)
{
  qDebug() << precision->toString();
  QSettings settings;
  settings.setValue("msms/precision", precision->toString());
  _precision_p = precision;
  emit msmsPrecisionChanged(_precision_p);
  updatePsm();
  qDebug();
}

void
SpectrumParam::tandemParamLaunchButtonPushed()
{
  qDebug();
  _tandem_param_p->hide();
  _tandem_param_p->show();
  qDebug();
}


void
SpectrumParam::setXtandemSpectrumProcess(
  XtandemSpectrumProcess xt_spectrum_process)
{
  qDebug();
  _spectrum_process = xt_spectrum_process;
  updatePsm();
  qDebug();
}
