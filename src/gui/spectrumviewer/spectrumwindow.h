
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/
#pragma once


#include <QMainWindow>
#include <QThread>
#include <QFileInfo>
#include <QMutex>
#include <QMessageBox>
#include <pappsomspp/types.h>
#include <pappsomspp/msrun/msrunid.h>
#include <pappsomspp/peptide/peptide.h>
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <ui_spectrumwindow.h>


namespace Ui
{
class SpectrumWindow;
}


class PwizLoaderThread : public QObject
{
  Q_OBJECT
  public:
  bool
  isLoading()
  {
    if(_mutex.tryLock())
      {
        _mutex.unlock();
        return false;
      }
    return true;
  }

  public slots:
  void doMsDataFileLoad(const QString &parameter);

  signals:
  void msDataReady(pappso::MsRunReaderSPtr msrun_reader);
  void msDataError(QString error);

  private:
  QMutex _mutex;
};

class SpectrumWindow : public QMainWindow
{
  Q_OBJECT
  QThread workerThread;

  public:
  explicit SpectrumWindow(QWidget *parent = 0);
  ~SpectrumWindow();

  void setMzDataFile(const QString &filepath);

  public slots:
  void setPeptideSp(pappso::PeptideSp peptide_sp);
  void selectMzFile();
  void saveSvgFile();
  void setMz(double mz);
  void setPeak(pappso::DataPointCstSPtr p_peak_match);
  void setIon(pappso::PeakIonIsotopeMatchCstSPtr ion);
  void setScanNum(int scan_num);
  void handleMsDataFile(pappso::MsRunReaderSPtr p_ms_data_file);
  void handleMsDataError(QString error);
  void setPrecision(pappso::PrecisionPtr p_precision);
  void setFilteredSpectrumSp(pappso::MassSpectrumSPtr filtered_spectrum);
  void setIsotopeMassList(
    std::vector<pappso::PeptideNaturalIsotopeAverageSp> isotopeMassList);
  void setFilteredCheckbox(bool checked);
  void setPeptideCharge(unsigned int charge);
  // void setColor(const QColor &color);
  // void setShape(Shape shape);
  signals:
  void spectrumChanged(pappso::QualifiedMassSpectrum spectrum);
  void operateMsDataFile(const QString &parameter);
  void mzChanged(double mz);

  private:
  Ui::SpectrumWindow *ui;
  QLabel *_mz_label;
  QLabel *_peak_label;
  QLabel *_ion_label;


  QFileInfo _mz_file_info;
  pappso::MsRunReaderSPtr _p_msrun_reader_sp = nullptr;

  bool _show_filtered_spectrum = true;
  pappso::QualifiedMassSpectrum _filtered_spectrum;
  pappso::QualifiedMassSpectrum _spectrum;

  pappso::MsRunIdCstSPtr _msrun_id;

  PwizLoaderThread *_worker_loader;
};
