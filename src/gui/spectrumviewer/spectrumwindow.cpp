
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "spectrumwindow.h"
#include <QFileDialog>
#include <QSettings>
#include <QSvgGenerator>
#include <QDebug>
#include <pappsomspp/peptide/peptidestrparser.h>
#include <pappsomspp/processing/filters/filterpseudocentroid.h>
#include <pappsomspp/msrun/private/timsmsrunreaderms2.h>
#include "ui_spectrumwindow.h"


void
PwizLoaderThread::doMsDataFileLoad(const QString &parameter)
{
  if(isLoading())
    return;
  _mutex.lock();
  pappso::MsRunReaderSPtr p_msreader;
  try
    {

      pappso::MsFileAccessor accessor(parameter, "a1");
      accessor.setPreferedFileReaderType(pappso::MzFormat::brukerTims,
                                         pappso::FileReaderType::tims_ms2);

      p_msreader = accessor.msRunReaderSp(accessor.getMsRunIds().front());

      pappso::TimsMsRunReaderMs2 *tims2_reader =
        dynamic_cast<pappso::TimsMsRunReaderMs2 *>(p_msreader.get());
      if(tims2_reader != nullptr)
        {
          qDebug();
          std::shared_ptr<pappso::FilterPseudoCentroid> ms2filter =
            std::make_shared<pappso::FilterPseudoCentroid>(60000, 0.05, 0.5, 0.1);

          tims2_reader->setMs2FilterCstSPtr(ms2filter);

          //((pappso::TimsMsRunReaderMs2 *)p_msreader.get())
          //  ->dontReadMs1Spectrum(true);
          qDebug();
        }
      _mutex.unlock();
      emit msDataReady(p_msreader);
    }

  catch(pappso::PappsoException exception_pappso)
    {
      qDebug() << "PwizLoaderThread::doMsDataFileLoad PappsoException "
               << exception_pappso.qwhat();
      _mutex.unlock();
      emit msDataError(exception_pappso.qwhat());
    }
  catch(std::exception exception_std)
    {
      qDebug() << "PwizLoaderThread::doMsDataFileLoad exception "
               << exception_std.what();
      _mutex.unlock();
      emit msDataError(QString(exception_std.what()));
    }
}


SpectrumWindow::SpectrumWindow(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::SpectrumWindow)
{
  ui->setupUi(this);

  _worker_loader = new PwizLoaderThread();
  _worker_loader->moveToThread(&workerThread);

  _mz_label = new QLabel("");
  ui->statusbar->addWidget(_mz_label);
  _peak_label = new QLabel("");
  ui->statusbar->addWidget(_peak_label);
  _ion_label = new QLabel("");
  ui->statusbar->addWidget(_ion_label);

  ui->spectrum_widget->setMaximumIsotopeNumber(3);

#if QT_VERSION >= 0x050000
  // Qt5 code
  connect(
    &workerThread, &QThread::finished, _worker_loader, &QObject::deleteLater);
  connect(this,
          &SpectrumWindow::operateMsDataFile,
          _worker_loader,
          &PwizLoaderThread::doMsDataFileLoad);
  connect(_worker_loader,
          &PwizLoaderThread::msDataReady,
          this,
          &SpectrumWindow::handleMsDataFile);
  /*
  connect(ui->spectrum_widget,
          &pappso::MassSpectrumWidget::mzChanged,
          this,
          &SpectrumWindow::setMz);
          */
  /*
  connect(ui->spectrum_widget,
          &pappso::MassSpectrumWidget::peakChanged,
          this,
          &SpectrumWindow::setPeak);
          */
  /*
  connect(ui->spectrum_widget,
          &pappso::MassSpectrumWidget::ionChanged,
          this,
          &SpectrumWindow::setIon);
          */
  connect(_worker_loader,
          &PwizLoaderThread::msDataError,
          this,
          &SpectrumWindow::handleMsDataError);

#else
  // Qt4 code
  connect(
    &workerThread, SIGNAL(finished()), _worker_loader, SLOT(deleteLater()));
  connect(this,
          SIGNAL(operateMsDataFile(QString)),
          _worker_loader,
          SLOT(doMsDataFileLoad(QString)));
  connect(_worker_loader,
          SIGNAL(msDataReady(pappso::MsRunReaderSp)),
          this,
          SLOT(handleMsDataFile(pappso::MsRunReaderSp)));
  connect(_worker_loader,
          SIGNAL(msDataError(QString)),
          this,
          SLOT(handleMsDataError(QString)));
  connect(
    _p_spectrum_overlay, SIGNAL(mzChanged(double)), this, SLOT(setMz(double)));

#endif
  /*
   */
  workerThread.start();
}

SpectrumWindow::~SpectrumWindow()
{
  workerThread.quit();
  workerThread.wait();
  delete ui;
}

void
SpectrumWindow::setPeptideSp(pappso::PeptideSp peptide_sp)
{
  qDebug();
  ui->spectrum_widget->setPeptideSp(peptide_sp);
  qDebug();
  // ui->spectrum_widget->replot();
  ui->spectrum_widget->plot();
  qDebug();
}


void
SpectrumWindow::setPeptideCharge(unsigned int charge)
{
  qDebug() << " charge=" << charge;
  ui->spectrum_widget->setPeptideCharge(charge);
  qDebug() << " charge=" << charge;
  ui->spectrum_widget->plot();
  qDebug() << " charge=" << charge;
}
void
SpectrumWindow::setPrecision(pappso::PrecisionPtr p_precision)
{

  qDebug();
  ui->spectrum_widget->setMs2Precision(p_precision);
  ui->spectrum_widget->plot();
  qDebug();
}


void
SpectrumWindow::setFilteredCheckbox(bool checked)
{
  qDebug();
  _show_filtered_spectrum = checked;
  if(_show_filtered_spectrum)
    {
      ui->spectrum_widget->setQualifiedMassSpectrum(_filtered_spectrum);
    }
  else
    {
      ui->spectrum_widget->setQualifiedMassSpectrum(_spectrum);
    }
  ui->spectrum_widget->plot();
  ui->spectrum_widget->rescale();
  qDebug();
}
void
SpectrumWindow::setFilteredSpectrumSp(
  pappso::MassSpectrumSPtr filtered_spectrum)
{

  qDebug();
  pappso::QualifiedMassSpectrum spectrum_copy(_spectrum);
  _filtered_spectrum = spectrum_copy;
  _filtered_spectrum.setMassSpectrumSPtr(filtered_spectrum);
  if(_show_filtered_spectrum)
    {
      ui->spectrum_widget->setQualifiedMassSpectrum(_filtered_spectrum);
      ui->spectrum_widget->plot();
      ui->spectrum_widget->rescale();
    }
  else
    {
      //  ui->spectrum_widget->setQualifiedSpectrum(_spectrum);
    }
  qDebug();
}

void
SpectrumWindow::setIon(pappso::PeakIonIsotopeMatchCstSPtr ion)
{
  qDebug();
  QString plusstr = "+";
  plusstr         = plusstr.repeated(ion->getCharge());
  _ion_label->setText(
    QString("%1%2%3 (+%4)")
      .arg(ion->getPeptideFragmentIonSp().get()->getPeptideIonName())
      .arg(ion->getPeptideFragmentIonSp().get()->size())
      .arg(plusstr)
      .arg(ion->getPeptideNaturalIsotopeAverageSp().get()->getIsotopeNumber()));
  qDebug();
}

void
SpectrumWindow::setPeak(pappso::DataPointCstSPtr p_peak_match)
{


  qDebug();
  if(p_peak_match == nullptr)
    {
      _peak_label->setText(QString("no peak"));
      _ion_label->setText("");
    }
  else
    {
      _peak_label->setText(QString("peak mz=%1 intensity=%2")
                             .arg(QString::number(p_peak_match->x, 'g', 10))
                             .arg(p_peak_match->y));
    }
  qDebug();
}

void
SpectrumWindow::setMz(double mz)
{
  if(mz > 0)
    {
      _mz_label->setText(QString("mz=%1").arg(QString::number(mz, 'g', 10)));
    }
  else
    {
      _mz_label->setText("");
    }
  emit mzChanged(mz);
}
void
SpectrumWindow::handleMsDataError(QString error)
{
  QMessageBox::warning(this, tr("Unable to read spectrum :"), error);
}

void
SpectrumWindow::handleMsDataFile(pappso::MsRunReaderSPtr p_ms_data_file)
{


  qDebug();

  QSettings settings;
  _p_msrun_reader_sp = p_ms_data_file;
  settings.setValue("path/mzfile", _mz_file_info.absolutePath());
  ui->mzFileName->setText(_mz_file_info.absoluteFilePath());

  _msrun_id = p_ms_data_file->getMsRunId();


  try
    {

      qDebug();
      _spectrum = _p_msrun_reader_sp.get()->qualifiedMassSpectrum(
        ui->scanNumSpinBox->value());
      qDebug() << " charge=" << _spectrum.getPrecursorCharge();
      pappso::MassSpectrumId spectrum_id = _spectrum.getMassSpectrumId();
      spectrum_id.setMsRunId(_msrun_id);
      _spectrum.setMassSpectrumId(spectrum_id);
      emit spectrumChanged(_spectrum);
    }
  catch(pappso::PappsoException exception)
    {
    }
  qDebug();
}

void
SpectrumWindow::setScanNum(int scan_num)
{
  qDebug();
  ui->scanNumSpinBox->setValue(scan_num);
  if(_p_msrun_reader_sp == nullptr)
    {
      return;
    }
  if(_worker_loader->isLoading())
    {
      return;
    }
  try
    {
      _spectrum = _p_msrun_reader_sp.get()->qualifiedMassSpectrum(scan_num);
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " charge=" << _spectrum.getPrecursorCharge();

      pappso::MassSpectrumId spectrum_id = _spectrum.getMassSpectrumId();
      spectrum_id.setMsRunId(_msrun_id);
      _spectrum.setMassSpectrumId(spectrum_id);
      ui->spectrum_widget->setQualifiedMassSpectrum(_spectrum);

      if(_spectrum.getMsLevel() == 1)
        {
          ui->filtered_checkbox->setDisabled(true);

          ui->spectrum_widget->plot();
          ui->spectrum_widget->rescale();
        }
      else
        {
          ui->filtered_checkbox->setDisabled(false);

          if(!_show_filtered_spectrum)
            {
              ui->spectrum_widget->plot();
              ui->spectrum_widget->rescale();
            }
        }
      emit spectrumChanged(_spectrum);
    }
  catch(pappso::PappsoException exception)
    {
    }
  qDebug();
}

void
SpectrumWindow::selectMzFile()
{

  qDebug();
  if(_worker_loader->isLoading())
    return;
  QSettings settings;
  QString default_location = settings.value("path/mzfile", "").toString();

  QString filename = QFileDialog::getOpenFileName(
    this,
    tr("Open mz File"),
    default_location,
    tr("mzXML files (*.mzXML);;mzML files (*.mzML);;MGF files (*.mgf);;all "
       "files (*)"));

  if(filename.isEmpty())
    {
      return;
    }
  setMzDataFile(filename);
  qDebug();
}

void
SpectrumWindow::setMzDataFile(const QString &filename)
{

  qDebug();
  QFileInfo new_mz_file;
  new_mz_file.setFile(filename);
  if(new_mz_file != _mz_file_info)
    {
      _mz_file_info.setFile(filename);

      ui->mzFileName->setText(tr("loading..."));
      emit operateMsDataFile(_mz_file_info.absoluteFilePath());
      //_p_ms_data_file = getPwizMSDataFile(_mz_file_info.absoluteFilePath());
    }
  qDebug();
}

void
SpectrumWindow::saveSvgFile()
{
  qDebug();
  QSettings settings;
  QString default_location = settings.value("path/save", "").toString();

  QString filename =
    QFileDialog::getSaveFileName(this,
                                 tr("Save SVG File"),
                                 default_location,
                                 tr("SVG files (*.svg);;all files (*)"));

  if(!filename.isEmpty())
    {
      ui->spectrum_widget->toSvgFile(
        filename,
        tr("PAPPSO MS tools SVG spectrum generator"),
        tr("This is an annotated SVG spectrum"),
        QSize(1200, 500));
    }
  qDebug();
}

void
SpectrumWindow::setIsotopeMassList(
  std::vector<pappso::PeptideNaturalIsotopeAverageSp> isotopeMassList)
{
  qDebug();
  // ui->spectrumWidget->setIsotopeMassList(isotopeMassList);
}
