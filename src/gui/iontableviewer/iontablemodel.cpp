
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "iontablemodel.h"


#include <QDebug>
#include <QFont>
#include <set>
#include <QBrush>
#include <pappsomspp/pappsoexception.h>


IonTableModel::IonTableModel(QObject *parent) : QAbstractTableModel(parent)
{

  // ui->tableView->show();
  // QModelIndex topLeft = createIndex(0,0);
  // emit a signal to make the view reread identified data
  // emit dataChanged(topLeft, topLeft);
  _p_precision = PrecisionFactory::getDaltonInstance(0.02);
}

void
IonTableModel::setMz(double mz)
{

  qDebug();
  // qDebug() << "IonTableModel::setMz " << mz;
  _current_mz             = mz;
  QModelIndex topLeft     = createIndex(0, 0);
  QModelIndex bottomRight = createIndex(rowCount(), columnCount());
  emit dataChanged(topLeft, bottomRight);
  qDebug();
}


void
IonTableModel::setPrecision(PrecisionPtr p_precision)
{
  qDebug();
  _p_precision = p_precision;
  setMaxCharge(_zmax);
  qDebug();
}

void
IonTableModel::setMaxCharge(unsigned int zmax)
{
  _zmax = zmax;
  qDebug();
  if(_peptide_ion_list_sp != nullptr)
    {
      setPeptideFragmentIonList(_peptide_ion_list_sp);
    }
  qDebug();
}


void
IonTableModel::setSpectrumSp(pappso::MassSpectrumSPtr spectrum_sp)
{
  qDebug();
  _spectrum_sp = spectrum_sp;
  qDebug();
}

void
IonTableModel::setPeptideFragmentIonList(
  PeptideFragmentIonListBaseSp &peptide_ion_list)
{
  qDebug();
  _peptide_ion_list_sp = peptide_ion_list;

  if(_peptide_ion_list_sp.get() == nullptr)
    {
      throw PappsoException(
        QObject::tr("_peptide_ion_list_sp.get() == nullptr"));
    }
  qDebug() << " " << _peptide_ion_list_sp.get();
  _phosphorylation_number =
    _peptide_ion_list_sp.get()->getPhosphorylationNumber();

  qDebug();
  _ion_cter.clear();
  _ion_nter.clear();

  // we identify the top left cell
  QModelIndex topLeft           = createIndex(0, 0);
  std::list<PeptideIon> ion_set = _peptide_ion_list_sp.get()->getIonList();

  QStringList header_list;
  _col_color.clear();
  header_list << "Nter size";
  _col_color.push_back(QColor("black"));
  // std::list<PeptideIon> ion_list(ion_set);
  qDebug();
  for(auto ion_type : ion_set)
    {
      PeptideDirection ion_direction =
        PeptideFragmentIon::getPeptideIonDirection(ion_type);
      if(ion_direction == PeptideDirection::Nter)
        {
          _ion_nter.push_back(ion_type);
          for(unsigned int z = 1; z <= _zmax; z++)
            {
              if(ion_type != PeptideIon::bp)
                {
                  header_list
                    << QString("%1 (z=%2)")
                         .arg(PeptideFragmentIon::getPeptideIonName(ion_type))
                         .arg(z);
                  _col_color.push_back(
                    PeptideFragmentIon::getPeptideIonColor(ion_type));
                }
              else
                {
                  for(unsigned int i = 0; i < _phosphorylation_number; i++)
                    {
                      header_list
                        << QString("%1%2 (z=%3)")
                             .arg(
                               PeptideFragmentIon::getPeptideIonName(ion_type))
                             .arg(i + 1)
                             .arg(z);
                      _col_color.push_back(
                        PeptideFragmentIon::getPeptideIonColor(ion_type));
                    }
                }
            }
        }
    }

  qDebug();
  // writer.writeCell("Sequence");
  _sequence_index = header_list.size();
  header_list << "Sequence";
  _col_color.push_back(QColor("black"));
  for(auto ion_type : ion_set)
    {
      PeptideDirection ion_direction =
        PeptideFragmentIon::getPeptideIonDirection(ion_type);
      if(ion_direction == PeptideDirection::Cter)
        {

          _ion_cter.push_back(ion_type);
          for(unsigned int z = 1; z <= _zmax; z++)
            {
              if(ion_type != PeptideIon::yp)
                {
                  header_list
                    << QString("%1 (z=%2)")
                         .arg(PeptideFragmentIon::getPeptideIonName(ion_type))
                         .arg(z);
                  _col_color.push_back(
                    PeptideFragmentIon::getPeptideIonColor(ion_type));
                }
              else
                {
                  for(unsigned int i = 0; i < _phosphorylation_number; i++)
                    {
                      header_list
                        << QString("%1%2 (z=%3)")
                             .arg(
                               PeptideFragmentIon::getPeptideIonName(ion_type))
                             .arg(i + 1)
                             .arg(z);
                      _col_color.push_back(
                        PeptideFragmentIon::getPeptideIonColor(ion_type));
                    }
                }
            }
        }
    }
  header_list << "Cter size";
  _col_color.push_back(QColor("black"));
  _header_list = header_list;
  // writer.writeCell("Cter size");
  // emit a signal to make the view reread identified data
  emit headerDataChanged(Qt::Horizontal, 0, _header_list.size());
  emit dataChanged(topLeft, topLeft);

  qDebug();
}


int
IonTableModel::rowCount(const QModelIndex &parent) const
{
  qDebug();
  if(_peptide_ion_list_sp.get() == nullptr)
    {
      return 0;
    }
  qDebug();
  return _peptide_ion_list_sp.get()->getPeptideSp().get()->size();
}
int
IonTableModel::columnCount(const QModelIndex &parent) const
{
  qDebug();
  if(_peptide_ion_list_sp.get() == nullptr)
    {
      return 0;
    }
  qDebug();
  return (_header_list.size());
}
QVariant
IonTableModel::headerData(int section,
                          Qt::Orientation orientation,
                          int role) const
{

  //qDebug();
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  switch(role)
    {
      case Qt::ForegroundRole:
        {
          if(orientation == Qt::Horizontal)
            {
              QBrush brush(_col_color[section], Qt::SolidPattern);
              return brush;
            }
          break;
        }

      case Qt::DisplayRole:
        {
          if(orientation == Qt::Horizontal)
            {
              if((_peptide_ion_list_sp.get() != nullptr) &&
                 (section < _header_list.size()))
                {
                  return _header_list[section];
                }
            }
        }
    }
  //qDebug();
  return QVariant();
}
QVariant
IonTableModel::data(const QModelIndex &index, int role) const
{
  qDebug();
  // generate a log message when this method gets called
  int row = index.row();
  int col = index.column();

  /*qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << QString(
                "IonTableModel::data row %1, col%2, role %3 Qt::FontRole %4")
                .arg(row)
                .arg(col)
                .arg(role)
                .arg(Qt::FontRole);*/
  int cter_size =
    _peptide_ion_list_sp.get()->getPeptideSp().get()->size() - index.row();
  unsigned int peptide_size =
    _peptide_ion_list_sp.get()->getPeptideSp().get()->size();

  switch(role)
    {
      case Qt::ForegroundRole:
        {
          unsigned int z = 0;
          const PeptideFragmentIon *p_peptide_fragment_ion =
            getFragmentIonPointer(index, &z);
          if(p_peptide_fragment_ion != nullptr)
            {
              // does it match ?
              if(_spectrum_sp != nullptr)
                {
                  bool match = false;
                  std::vector<DataPoint>::const_iterator it_peak =
                    _spectrum_sp.get()->begin();
                  MzRange mz_range(p_peptide_fragment_ion->getMz(z),
                                   _p_precision);
                  while(it_peak != _spectrum_sp.get()->end())
                    {
                      if(mz_range.contains(it_peak->x))
                        {
                          match = true;
                        }
                      it_peak++;
                    }
                  if(match)
                    {
                      QBrush brush(
                        PeptideFragmentIon::getPeptideIonColor(
                          p_peptide_fragment_ion->getPeptideIonType()),
                        Qt::SolidPattern);
                      return brush;
                    }
                }
            }
          return QVariant();
          break;
        }
      case Qt::TextAlignmentRole:
        {
          unsigned int z = 0;
          const PeptideFragmentIon *p_peptide_fragment_ion =
            getFragmentIonPointer(index, &z);
          if(p_peptide_fragment_ion == nullptr)
            {
              return (Qt::AlignHCenter);
            }
          else
            {
              return (Qt::AlignRight);
            }
          return QVariant();
          break;
        }
      case Qt::FontRole:
        {
          QFont boldFont;
          boldFont.setBold(true);
          pappso::MzRange mass_range(_current_mz,
                                     PrecisionFactory::getDaltonInstance(2));
          // qDebug() << "mass_range = " << mass_range.getMz() << " NO "
          //          << index.column() << " " << index.row();
          if(row == 0 && col == 0)
            return boldFont;
          unsigned int z = 0;
          const PeptideFragmentIon *p_peptide_fragment_ion =
            getFragmentIonPointer(index, &z);
          if(p_peptide_fragment_ion != nullptr)
            {

              /*qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                       << "mass_range = " << mass_range.getMz() << " "
                       << p_peptide_fragment_ion->getMz(z);*/
              if(mass_range.contains(p_peptide_fragment_ion->getMz(z)))
                {
                  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " <<
                  // __LINE__;
                  return boldFont;
                }
            }
          else
            {

              // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
              //         << "mass_range = " << mass_range.getMz() << " NO "
              //         << index.column() << " " << index.row();
            }
          return QVariant();
          break;
        }
      case Qt::DisplayRole:
        {
          unsigned int z = 0;
          const PeptideFragmentIon *p_peptide_fragment_ion =
            getFragmentIonPointer(index, &z);
          if(p_peptide_fragment_ion != nullptr)
            {
              return (QString("%1").arg(
                QString::number(p_peptide_fragment_ion->getMz(z), 'g', 10)));
            }
          if(index.column() == 0)
            { // Nter size
              // return
              //    _current_directory.entryInfoList().at(index.row()).fileName();

              if((index.row() + 1) == peptide_size)
                return QVariant();

              return QString("%1").arg(index.row() + 1);
            }
          else if(index.column() == _sequence_index)
            { // sequence
              return QString("%1").arg(_peptide_ion_list_sp.get()
                                         ->getPeptideSp()
                                         .get()
                                         ->getConstAa(index.row())
                                         .toString());
            }
          else if(index.column() == _header_list.size() - 1)
            { // Cter size

              if(cter_size == peptide_size)
                return QVariant();
              return QString("%1").arg(peptide_size - index.row());
            }
        }
    }
  qDebug();
  return QVariant();
}

const pappso::PeptideFragmentIon *
IonTableModel::getFragmentIonPointer(const QModelIndex &index,
                                     unsigned int *p_z) const
{
  qDebug();
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  unsigned int peptide_size =
    _peptide_ion_list_sp.get()->getPeptideSp().get()->size();
  int cter_size =
    _peptide_ion_list_sp.get()->getPeptideSp().get()->size() - index.row();

  pappso::PeptideFragmentIon *p_fragment_ion = nullptr;

  if(index.column() == 0)
    { // Nter size
      return p_fragment_ion;
    }
  else if(index.column() < _sequence_index)
    { // Nter ion
      if((index.row() + 1) == peptide_size)
        return p_fragment_ion;
      unsigned int index_col = 1;
      for(auto ion_type : _ion_nter)
        {
          for(unsigned int z = 1; z <= _zmax; z++)
            {
              *p_z = z;
              if(ion_type != PeptideIon::bp)
                {
                  if(index_col == index.column())
                    {

                      return (
                        _peptide_ion_list_sp.get()
                          ->getPeptideFragmentIonSp(ion_type, index.row() + 1)
                          .get());
                    }
                  index_col++;
                }
              else
                {
                  for(unsigned int i = 0; i < _phosphorylation_number; i++)
                    {

                      qDebug() << __FILE__ << " " << __FUNCTION__ << " "
                               << __LINE__ << i;
                      if(index_col == index.column())
                        {
                          return (_peptide_ion_list_sp.get()
                                    ->getPeptideFragmentIonSp(
                                      ion_type, index.row() + 1, i + 1)
                                    .get());
                        }
                      index_col++;
                    }
                }
            }
        }
    }
  else if(index.column() == _sequence_index)
    { // sequence
      return p_fragment_ion;
    }
  else if(index.column() == _header_list.size() - 1)
    { // Cter size
      return p_fragment_ion;
    }
  else if(index.column() > _sequence_index)
    { // Cter ion
      if(cter_size == peptide_size)
        return p_fragment_ion;
      unsigned int index_col = _sequence_index + 1;
      for(auto ion_type : _ion_cter)
        {
          for(unsigned int z = 1; z <= _zmax; z++)
            {
              *p_z = z;
              if(ion_type != PeptideIon::yp)
                {
                  if(index_col == index.column())
                    {
                      return (_peptide_ion_list_sp.get()
                                ->getPeptideFragmentIonSp(ion_type, cter_size)
                                .get());
                    }
                  index_col++;
                }
              else
                {
                  for(unsigned int i = 0; i < _phosphorylation_number; i++)
                    {
                      if(index_col == index.column())
                        {
                          return (_peptide_ion_list_sp.get()
                                    ->getPeptideFragmentIonSp(
                                      ion_type, cter_size, i + 1)
                                    .get());
                        }
                      index_col++;
                    }
                }
            }
        }
    }

  qDebug();
  return p_fragment_ion;
}
