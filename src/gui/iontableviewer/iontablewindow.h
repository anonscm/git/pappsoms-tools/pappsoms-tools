
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QMainWindow>
#include <pappsomspp/peptide/peptidefragmentionlistbase.h>
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include "iontablemodel.h"

namespace Ui
{
class IonTableWindow;
}

using namespace pappso;

class IonTableWindow : public QMainWindow
{
  Q_OBJECT

  public:
  explicit IonTableWindow(QWidget *parent = 0);
  ~IonTableWindow();

  public slots:
  void setPeptideFragmentIonList(PeptideFragmentIonListBaseSp peptide_ion_list);
  void setSpectrum(pappso::QualifiedMassSpectrum spectrum);
  void setPeptideCharge(unsigned int charge);
  void setMz(double mz);
  void setPrecision(PrecisionPtr p_precision);

  private:
  Ui::IonTableWindow *ui;

  IonTableModel *_p_ion_table_model;
  pappso::QualifiedMassSpectrum _spectrum;
};
