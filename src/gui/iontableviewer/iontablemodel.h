
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QAbstractTableModel>
#include <QStringList>
#include <QColor>
#include <pappsomspp/peptide/peptidefragmentionlistbase.h>
#include <pappsomspp/massspectrum/massspectrum.h>

using namespace pappso;

class IonTableModel : public QAbstractTableModel
{
  Q_OBJECT
  public:
  IonTableModel(QObject *parent = 0);

  void
  setPeptideFragmentIonList(PeptideFragmentIonListBaseSp &peptide_ion_list);
  void setPrecision(PrecisionPtr p_precision);
  void setMz(double mz);
  void setMaxCharge(unsigned int zmax);

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant
  headerData(int section, Qt::Orientation orientation, int role) const override;
  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;
  void setSpectrumSp(pappso::MassSpectrumSPtr spectrum_sp);

  private:
  const pappso::PeptideFragmentIon *
  getFragmentIonPointer(const QModelIndex &index, unsigned int *p_z) const;

  private:
  PeptideFragmentIonListBaseSp _peptide_ion_list_sp;
  QStringList _header_list;
  unsigned int _zmax = 1;
  std::list<PeptideIon> _ion_nter;
  std::list<PeptideIon> _ion_cter;
  std::vector<QColor> _col_color;
  unsigned int _phosphorylation_number;
  unsigned int _sequence_index;
  pappso::MassSpectrumCstSPtr _spectrum_sp = nullptr;
  PrecisionPtr _p_precision;

  double _current_mz = 0;
};
