
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "iontablewindow.h"
#include "ui_iontablewindow.h"
#include <QDebug>

IonTableWindow::IonTableWindow(QWidget *parent)
  : QMainWindow(parent), ui(new Ui::IonTableWindow)
{
  ui->setupUi(this);

#if QT_VERSION >= 0x050000
  // Qt5 code
  // connect(&workerThread, &QThread::finished, worker, &QObject::deleteLater);

#else
  // Qt4 code
  // connect(&workerThread, SIGNAL(finished()), worker, SLOT(deleteLater()));

#endif
  /*
   */

  _p_ion_table_model = new IonTableModel();

  ui->ionTableView->setModel(_p_ion_table_model);
}

IonTableWindow::~IonTableWindow()
{
  delete ui;
}

void
IonTableWindow::setPeptideFragmentIonList(
  PeptideFragmentIonListBaseSp peptide_ion_list)
{
  qDebug();
  _p_ion_table_model->setPeptideFragmentIonList(peptide_ion_list);
  qDebug();
}


void
IonTableWindow::setPrecision(PrecisionPtr p_precision)
{
  qDebug();
  _p_ion_table_model->setPrecision(p_precision);
  qDebug();
}


void
IonTableWindow::setPeptideCharge(unsigned int charge)
{
  qDebug() << " charge=" << charge;
  if(charge > 0)
    {
      _p_ion_table_model->setMaxCharge(charge);
    }
  qDebug() << " charge=" << charge;
}


void
IonTableWindow::setSpectrum(pappso::QualifiedMassSpectrum spectrum)
{
  qDebug();
  _spectrum = spectrum;
  _p_ion_table_model->setSpectrumSp(_spectrum.getMassSpectrumSPtr());
  //_p_ion_table_model->setMaxCharge(_spectrum.getPrecursorCharge());
  qDebug();
}

void
IonTableWindow::setMz(double mz)
{
  qDebug();
  _p_ion_table_model->setMz(mz);
  qDebug();
}
