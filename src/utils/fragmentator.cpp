
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of PAPPSOms-tools.
 *
 *     PAPPSOms-tools is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms-tools is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms-tools.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "fragmentator.h"
#include <QDebug>

Fragmentator::Fragmentator()
{
  _ion_list = PeptideFragmentIonListBase::getCIDionList();
}

void
Fragmentator::setPeptideSp(pappso::PeptideSp peptide_sp)
{
  qDebug() << (peptide_sp != nullptr);
  _peptide_sp = peptide_sp;
  PeptideFragmentIonListBase peptide_ion_list(_peptide_sp, _ion_list);
  qDebug();
  emit peptideFragmentIonListChanged(
    peptide_ion_list.makePeptideFragmentIonListBaseSp());
  qDebug();
}
